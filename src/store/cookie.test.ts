/**
 * @vitest-environment jsdom
 */

import { expect, test } from 'vitest';
import { cookie } from './cookie';

test('cookie', async (): Promise<void> => {
  const key = 'key';
  const value = `${Date.now()}`;
  cookie.set(key, value);
  cookie.set(key, `${value}-1`, '/test', 'qq.com', 3);
  expect(document.cookie).toEqual(`${key}=${value}`);
  expect(cookie.get(key)).toEqual(value);
  cookie.delete(key, '/test', 'qq.com');
  expect(cookie.get(key)).toEqual(value);
  cookie.delete([key]);
  expect(cookie.get(key)).toEqual('');
});

import { isArray } from '../type';

/** Cookie 操作 */
export const cookie = {
  /**
   * 查询 Cookie
   * @param name Cookie 名
   * @returns Cookie 值
   */
  get(name: string): string {
    const value: string = document.cookie.match(new RegExp(`(\\b)${name}=([^;]*)(;|$)`))?.[2];
    return !value ? '' : decodeURIComponent(value);
  },

  /**
   * 设置 Cookie
   * @param name Cookie 名
   * @param value Cookie 值
   * @param path 存放路径
   * @param domain 域名
   * @param day 有效日期
   */
  set(name: string, value: any, path?: string, domain?: string, day?: number): void {
    const maxAge: string = day > 0 ? `Max-Age=${86400 * day};` : '';
    path = `path=${path || '/'};`;
    domain = domain ? `domain=${domain};` : '';
    document.cookie = `${name}=${value}; ${maxAge} ${path} ${domain}`;
  },

  /**
   * 删除 Cookie
   * @param name Cookie 名
   * @param path 存放路径
   * @param domain 域名
   */
  delete(name: string | string[], path?: string, domain?: string): void {
    if (!isArray(name)) {
      name = [name];
    }
    path = `path=${path ? path : '/'};`;
    domain = domain ? `domain=${domain};` : '';
    name.forEach((key: string): void => {
      document.cookie = `${key}=; Max-Age=0; ${path} ${domain}`;
    });
  },
};

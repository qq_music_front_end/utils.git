export interface StorageStore {
  get(key: string): string;
  set(key: string, value: string): Error;
  delete(key: string): Error;
}

export interface StorageOptions {
  error?: boolean;
}

/** Storage */
export const storage = {
  use(store?: Storage, options?: StorageOptions): StorageStore {
    if (!store) {
      store = localStorage;
    }
    return {
      /**
       * 查询数据
       * @param key 要查询的键名
       * @returns 键名对应的值
       */
      get(key: string): string {
        try {
          return store.getItem(key);
        } catch (ex) {
          if (options?.error) {
            throw ex;
          }
          return '';
        }
      },
      /**
       * 保存数据
       * @param key 要创建或更新的键名
       * @param value 要创建或更新的键名对应的值
       * @returns 发生异常时返回错误信息
       */
      set(key: string, value: string): Error {
        try {
          store.setItem(key, value);
        } catch (ex) {
          if (options?.error) {
            throw ex;
          }
          return ex;
        }
      },
      /**
       * 删除数据
       * @param key 要删除的键名
       * @returns 发生异常时返回错误信息
       */
      delete(key: string): Error {
        try {
          store.removeItem(key);
        } catch (ex) {
          if (options?.error) {
            throw ex;
          }
          return ex;
        }
      },
    };
  },
};

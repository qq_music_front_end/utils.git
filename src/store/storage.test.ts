/**
 * @vitest-environment jsdom
 */

import { describe, expect, test } from 'vitest';
import { StorageStore, storage } from './storage';

describe.concurrent('storage', (): void => {
  const key = 'key';
  const value = `${Date.now()}`;

  function testStorage(originStore?: Storage): void {
    const store: StorageStore = storage.use(originStore);
    store.set(key, value);
    expect((originStore || localStorage).getItem(key)).toEqual(value);
    expect(store.get(key)).toEqual(value);
    store.delete(key);
    expect(store.get(key)).toBeNull();
  }

  test('localStorage', testStorage.bind(null, null));
  test('sessionStorage', testStorage.bind(null, sessionStorage));
  test('不抛出异常', (): void => {
    const store: StorageStore = storage.use('error' as any);
    expect(store.set(key, value) instanceof Error).toEqual(true);
    expect(store.get(key)).toEqual('');
    expect(store.delete(key) instanceof Error).toEqual(true);
  });
  test('抛出异常', (): void => {
    const store: StorageStore = storage.use('error' as any, { error: true });
    expect((): Error => store.set(key, value)).toThrowError('setItem');
    expect((): string => store.get(key)).toThrowError('getItem');
    expect((): Error => store.delete(key)).toThrowError('removeItem');
  });
});

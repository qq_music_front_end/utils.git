import { expect, test } from 'vitest';
import { MemoryStore, memory } from './memory';

test('memory', (): void => {
  const storeKey = 'store';
  const key = 'key';
  const value: number = Date.now();
  const store: MemoryStore<number> = memory.use(storeKey);
  store.set(key, value);
  expect(store.get(key)).toEqual(value);
  store.delete(key);
  expect(store.has(key)).toEqual(false);
  expect(memory.use(storeKey) === store).toEqual(true);
});

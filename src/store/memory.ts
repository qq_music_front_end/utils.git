export type MemoryStore<T = any> = Map<string, T>;

const store: Record<string, MemoryStore> = {};

/**
 * 内存缓存
 */
export const memory = {
  use<T = any>(key: string): MemoryStore<T> {
    if (!store[key]) {
      store[key] = new Map();
    }
    return store[key];
  }
}

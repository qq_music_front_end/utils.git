/**
 * @vitest-environment jsdom
 */

import { IDBFactory } from 'fake-indexeddb';
import 'fake-indexeddb/auto';
import { expect, test } from 'vitest';
import { IDBStore, idb } from './idb';

test('idb', async (): Promise<void> => {
  window.indexedDB = new IDBFactory();
  const database = 'db';
  const table = 'table';
  const key = 'key';
  const value = `${Date.now()}`;
  const store: IDBStore = idb.use(database, table);
  await store.set(key, value);
  expect(await store.get(key)).toEqual(value);
  await store.delete(key);
  expect(await store.get(key)).toBeUndefined();
});

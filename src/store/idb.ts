export interface IDBStore<T = any> {
  get(key: string): Promise<T>;
  set(key: string, value: T): Promise<void>;
  delete(key: string): Promise<void>;
}

interface OpenRes {
  db: IDBDatabase;
  transaction: IDBTransaction;
  store: IDBObjectStore;
}

async function open(dbName: string, storeName: string, mode?: IDBTransactionMode, version?: number): Promise<OpenRes> {
  const db: IDBDatabase = await new Promise<IDBDatabase>(
    (resolve: (value: IDBDatabase) => void, reject: (reason?: any) => void): void => {
      const request: IDBOpenDBRequest = indexedDB.open(dbName, version);
      request.addEventListener('error', reject);
      request.addEventListener('upgradeneeded', (): void => {
        const { result } = request;
        if (!result.objectStoreNames.contains(storeName)) {
          result.createObjectStore(storeName, {
            keyPath: 'id',
            autoIncrement: true,
          });
        }
      });
      request.addEventListener('success', (): void => {
        resolve(request.result);
      });
    },
  );
  if (!db.objectStoreNames.contains(storeName)) {
    // 更新数据库版本重新打开
    db.close();
    return await open(dbName, storeName, mode, db.version + 1);
  }
  const transaction: IDBTransaction = db.transaction(storeName, mode || 'readonly');
  const store: IDBObjectStore = transaction.objectStore(storeName);
  return { db, transaction, store };
}

async function doRequest<T>(db: IDBDatabase, request: IDBRequest): Promise<T> {
  return new Promise((resolve: (value: T) => void, reject: (reason?: any) => void): void => {
    request.addEventListener('error', reject);
    request.addEventListener('success', (): void => {
      resolve(request.result?.data);
      db.close();
    });
  });
}

/** indexedDB */
export const idb = {
  use<T = any>(dbName: string, storeName: string): IDBStore {
    return {
      async get<T>(key: string): Promise<T> {
        const { db, store } = await open(dbName, storeName);
        return await doRequest<T>(db, store.get(key));
      },
      async set(key: string, value: T): Promise<void> {
        const { db, store } = await open(dbName, storeName, 'readwrite');
        await doRequest(db, store.put({ id: key, data: value }));
      },
      async delete(key: string): Promise<void> {
        const { db, store } = await open(dbName, storeName, 'readwrite');
        await doRequest(db, store.delete(key));
      },
    };
  },
};

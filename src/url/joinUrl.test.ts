import { expectRun } from '../test';
import { joinUrl } from './joinUrl';

expectRun({
  name: 'joinUrl',
  handle(source: string[]): string {
    return joinUrl(...source);
  },
  cases: [
    {
      name: '合并路径',
      source: ['aaa', 'bb', null, 'c/d.html'],
      target: 'aaa/bb/c/d.html',
    },
    {
      name: '带协议头',
      source: ['https://y.qq.com/', '/m/', '/lib/h5/music.js'],
      target: 'https://y.qq.com/m/lib/h5/music.js',
    },
  ],
});

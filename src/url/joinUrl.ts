import { toString } from '../object';

function normalPath(path: string): string {
  return path.replace(/[\\/]+/g, '/');
}

/** 合并链接 */
export function joinUrl(...paths: string[]): string {
  const pathList: string[] = [];
  paths.forEach((item: string): void => {
    item = toString(item).trim();
    if (item) {
      pathList.push(item);
    }
  });
  const path: string = pathList.join('/').replace(/\\/g, '/');
  const url: RegExpMatchArray = path.match(/^(\w*:)[\\/]{2,}(.*)$/);
  if (url) {
    const [, protocol, subPath] = url;
    return `${protocol}//${normalPath(subPath)}`;
  }
  return normalPath(path);
}

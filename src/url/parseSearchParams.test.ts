import { expectRun } from '../test';
import { parseSearchParams } from './parseSearchParams';

expectRun({
  name: 'parseSearchParams',
  handle: parseSearchParams,
  cases: [
    {
      name: '带问号',
      source: '?ver=1&s=%2d',
      target: {
        ver: '1',
        s: '-',
      },
    },
    {
      name: '重复参数',
      source: 'ver=1&s=%2d&&ver=2&d',
      target: {
        ver: '1',
        s: '-',
        d: '',
      },
    },
    {
      name: '空对象',
      source: '',
      target: {},
    },
  ],
});

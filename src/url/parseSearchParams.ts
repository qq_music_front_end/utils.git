/** 解析URL参数 */
export function parseSearchParams(query: string): Record<string, string> {
  const params: Record<string, string> = {};
  if (query) {
    query
      .replace(/^\?/, '')
      .split('&')
      .forEach((item: string): void => {
        if (!item) {
          return;
        }
        const index: number = item.indexOf('=');
        const name: string = decodeURIComponent(index === -1 ? item : item.substring(0, index));
        if (params.hasOwnProperty(name)) {
          return;
        }
        params[name] = index === -1 ? '' : decodeURIComponent(item.substring(index + 1));
      });
  }
  return params;
}

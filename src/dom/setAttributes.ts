import { isArray, isFunction, isObject } from '../type';

/**
 * 设置节点属性
 * @param element
 * @param props
 * @returns
 */
export function setAttributes(element: Element, props?: Record<string, any>): void {
  if (!props || !isObject(props)) {
    return;
  }
  for (const key in props) {
    let value: any = props[key];
    if (key === 'namespaceURI' || !props.hasOwnProperty(key) || value == null) {
      continue;
    }
    if (/^on[A-Z]\w+$/.test(key) && isFunction(value)) {
      const eventName: string = key.slice(2).toLowerCase();
      element.addEventListener(eventName, value);
      continue;
    }
    if (isArray(value)) {
      value = value.join(' ');
    } else if (isObject(value)) {
      if (key === 'style') {
        const styleList: string[] = [];
        for (const styleKey in value) {
          const styleValue: any = value[styleKey];
          if (styleKey && styleValue != null) {
            styleList.push(`${styleKey}:${styleValue}`);
          }
        }
        value = styleList.join(';');
      } else {
        value = JSON.stringify(value);
      }
    }
    element.setAttribute(key === 'className' ? 'class' : key, value);
  }
}

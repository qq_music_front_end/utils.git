/**
 * @vitest-environment jsdom
 */

import { expect, test } from 'vitest';
import { createElement } from './createElement';
import { togglePostion } from './togglePostion';

test('togglePostion', (): void => {
  const domA: HTMLElement = createElement('span', {}, 'A');
  const domB: HTMLElement = createElement('i', {}, 'B');
  const tree: HTMLElement = createElement(
    'div',
    {},
    domA,
    createElement('hr'),
    createElement('div', {}, createElement('i', {}, ':'), domB),
  );
  togglePostion(domA, domA);
  togglePostion(domA, domB);
  const html = '<div><i>B</i><hr><div><i>:</i><span>A</span></div></div>';
  expect(tree.outerHTML).toEqual(html);
});

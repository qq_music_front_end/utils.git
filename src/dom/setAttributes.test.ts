/**
 * @vitest-environment jsdom
 */

import { expect, test } from 'vitest';
import { setAttributes } from './setAttributes';

test('setAttributes', (): void => {
  const dom: HTMLDivElement = document.createElement('div');
  setAttributes(dom);
  setAttributes(dom, {
    id: 'wrap',
    className: ['header', 'fix'],
    style: {
      background: '#ffffff',
      color: '#000000',
      display: null,
    },
    'data-info': { info: 1 },
    nullProps: null,
    onClick: console.log,
  });
  const html =
    '<div id="wrap" class="header fix" style="background:#ffffff;color:#000000" data-info="{&quot;info&quot;:1}"></div>';
  expect(dom.outerHTML).toEqual(html);
});

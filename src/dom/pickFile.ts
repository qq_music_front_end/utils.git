import { createElement } from './createElement';

/**
 * 选取文件
 * @param accept 文件类型
 * @returns
 */
export async function pickFile(accept?: string): Promise<FileList> {
  return new Promise((resolve: (value: FileList) => void): void => {
    function onChange(event: Event): void {
      const target = event.target as HTMLInputElement;
      target.removeEventListener('change', onChange);
      target.remove();
      resolve(target.files);
    }
    const input: HTMLInputElement = createElement('input', {
      type: 'file',
      style: 'display:none',
      accept,
      onChange,
    });
    document.body.append(input);
    input.click();
  });
}

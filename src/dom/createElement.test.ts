/**
 * @vitest-environment jsdom
 */

import { Mock, expect, test, vi } from 'vitest';
import { createElement } from './createElement';

test('createElement', (): void => {
  const clickHandle: Mock = vi.fn();
  const dom: HTMLElement = createElement(
    'div',
    {
      style: { display: 'block', color: '#f00' },
      'data-id': { aa: 12 },
      onClick: clickHandle,
    },
    createElement('hr'),
    [
      createElement(
        'svg',
        {
          namespaceURI: 'http://www.w3.org/2000/svg',
        },
        createElement('use', {
          namespaceURI: 'http://www.w3.org/2000/svg',
          href: '#user',
        }),
      ),
      createElement('span', {
        style: 'display:block',
        'data-id': 1234,
      }),
      null,
      [33423, 'fdsfds'],
      'end',
    ],
  );
  const html =
    '<div style="display:block;color:#f00" data-id="{&quot;aa&quot;:12}"><hr><svg><use href="#user"></use></svg><span style="display:block" data-id="1234"></span>33423fdsfdsend</div>';
  expect(dom.outerHTML).toEqual(html);
  dom.click();
  expect(clickHandle).toHaveBeenCalled();
});

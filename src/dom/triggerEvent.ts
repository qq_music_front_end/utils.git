export interface EventInit {
  /** 是否冒泡，默认为 false */
  bubbles?: boolean;
  /** 能否被取消，默认为 false */
  cancelable?: boolean;
  /** 是否会在影子DOM根节点之外触发侦听器，默认为 false */
  composed?: boolean;
}

/**
 * 触发 DOM 事件
 * @param elem 要触发事件的元素节点
 * @param eventName 事件类型
 * @param eventInit 事件设置
 */
export function triggerEvent(elem: Element | Window, eventName: string, eventInit?: EventInit): void {
  if (elem?.dispatchEvent && eventName) {
    const event = new Event(eventName, eventInit);
    elem.dispatchEvent(event);
  }
}

/**
 * 查询dom节点
 * 基于querySelector封装，支持指定parent
 * @param selectors 选择器
 * @param parent 父节点，默认为 document
 * @returns
 */
export function querySelector<T extends Element>(selectors: string, parent?: Document | Element): T {
  if (!parent?.querySelector) {
    parent = document;
  }
  return parent.querySelector(selectors);
}

/**
 * 查询dom节点列表
 * 基于querySelector封装，支持指定parent
 * @param selectors 选择器
 * @param parent 父节点，默认为 document
 * @returns
 */
export function querySelectorAll<T extends Element>(selectors: string, parent?: Document | Element): NodeListOf<T> {
  if (!parent?.querySelectorAll) {
    parent = document;
  }
  return parent.querySelectorAll(selectors);
}

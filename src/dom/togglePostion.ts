import { createElement } from './createElement';

/**
 * 交换两个 DOM 元素位置
 * @param a
 * @param b
 */
export function togglePostion(a: Element, b: Element): void {
  if (a && b && a != b) {
    const placeholder: HTMLElement = createElement('i', { style: 'display:none' });
    a.replaceWith(placeholder);
    b.replaceWith(a);
    placeholder.replaceWith(b);
  }
}

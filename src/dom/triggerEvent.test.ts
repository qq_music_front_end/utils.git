/**
 * @vitest-environment jsdom
 */

import { Mock, expect, test, vi } from 'vitest';
import { createElement } from './createElement';
import { triggerEvent } from './triggerEvent';

test('togglePostion', (): void => {
  const mockFn1: Mock = vi.fn();
  const mockFn2: Mock = vi.fn();
  const dom: HTMLElement = createElement('div', {
    onClick: mockFn1,
  });
  document.body.append(dom);
  document.addEventListener('click', mockFn2);
  triggerEvent(dom, 'click', {
    bubbles: true,
  });
  expect(mockFn1).toHaveBeenCalled();
  expect(mockFn2).toHaveBeenCalled();
});

/**
 * @vitest-environment jsdom
 */

import { expect, test } from 'vitest';
import { querySelector, querySelectorAll } from './querySelector';

test('querySelector', (): void => {
  document.body.innerHTML = '<main><ul><li>1</li><li>2</li><li>3</li></ul><section>test</section></main>';
  const dom: HTMLElement | null = document.querySelector('li');
  expect(querySelector('li')).toEqual(dom);
  expect(querySelector('li', document.body)).toEqual(dom);
});

test('querySelectorAll', (): void => {
  document.body.innerHTML = '<main><ul><li>1</li><li>2</li><li>3</li></ul></main>';
  const dom: NodeListOf<HTMLLIElement> = document.querySelectorAll('li');
  expect(querySelectorAll('li')).toEqual(dom);
  expect(querySelectorAll('li', document.body)).toEqual(dom);
});

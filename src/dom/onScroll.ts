import { isFunction } from '../type';
import { triggerEvent } from './triggerEvent';

let handles: ScrollHandle[] = null;

export interface ScrollHandle {
  (top: number, windowHeight: number, scrollHeight: number): void;
}

function initScrollEvent(): void {
  if (handles === null) {
    handles = [];
    window.addEventListener('scroll', (): void => {
      const { innerHeight: windowHeight } = window;
      const { body, documentElement } = document;
      const top: number = documentElement.scrollTop + body.scrollTop;
      const scrollHeight: number = documentElement.scrollHeight || body.scrollHeight;
      handles.forEach((handle: ScrollHandle): void => {
        handle(top, windowHeight, scrollHeight);
      });
    });
  }
}

/**
 * 监听页面滚动事件，不传处理函数时直接触发滚动事件
 * @param handle
 * @returns
 */
export function onScroll(handle?: ScrollHandle): void {
  // 监听滚动事件
  if (isFunction(handle)) {
    initScrollEvent();
    handles.push(handle);
    return;
  }
  // 触发滚动事件
  if (handles) {
    triggerEvent(window, 'scroll');
  }
}

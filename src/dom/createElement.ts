import { isArray } from '../type';
import { setAttributes } from './setAttributes';

export type ElementChild = ElementChild[] | Node | Node[] | string | number | null;

function normalizeChildren(...children: ElementChild[]): (string | Node)[] {
  const result: (string | Node)[] = [];
  children.forEach((item: ElementChild): void => {
    if (item == null) {
      return;
    }
    if (isArray(item)) {
      result.push(...normalizeChildren(...item));
      return;
    }
    if (item instanceof Node) {
      result.push(item);
      return;
    }
    result.push('' + item);
  });
  return result;
}

/**
 * 创建DOM节点
 * @param tagName 标签名
 * @param props 节点属性
 * @param children 子元素
 * @example
 *  createElement(
      'div',
      {
        style: { display: 'block', color: '#f00' },
        'data-id': { aa: 12 }
      },
      createElement('hr'),
      [
        createElement('span', {
          style: 'display:block',
          'data-id': 1234
        }),
        null,
        [33423, 'fdsfds'],,
        'end'
      ]
    );

    <div style="display:block;color:#f00" data-id="{&quot;aa&quot;:12}"><hr><span style="display:block" data-id="1234"></span>33423fdsfdsend</div>
 */
export function createElement<T extends Element = HTMLElement>(
  tagName: string,
  props?: Record<string, any>,
  ...children: ElementChild[]
): T {
  const ns: string = props?.namespaceURI;
  const element: Element = ns ? document.createElementNS(ns, tagName) : document.createElement(tagName);
  setAttributes(element, props);
  if (children.length) {
    element.append(...normalizeChildren(...children));
  }
  return element as T;
}

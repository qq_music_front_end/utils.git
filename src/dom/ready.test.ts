/**
 * @vitest-environment jsdom
 */

import { Mock, expect, test, vi } from 'vitest';
import { ready } from './ready';

test('ready', async (): Promise<void> => {
  const mockFn: Mock = vi.fn();
  await ready().then(mockFn);
  expect(mockFn).toHaveBeenCalled();
});

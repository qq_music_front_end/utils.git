const loading = 'loading';
let wait: Promise<boolean>;

async function onReady(): Promise<boolean> {
  return new Promise((resolve: (value: boolean) => void): void => {
    document.addEventListener('readystatechange', (): void => {
      if (document.readyState != loading) {
        resolve(true);
        wait = null;
      }
    });
  });
}

/** 等待页面可交互 */
export async function ready(): Promise<boolean> {
  if (document.readyState != loading) {
    return;
  }
  if (!wait) {
    wait = onReady();
  }
  return wait;
}

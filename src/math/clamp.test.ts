/**
 * @vitest-environment jsdom
 */

import { clamp } from './clamp';
import { expect, test } from 'vitest';

test('clamp', async (): Promise<void> => {
  expect(clamp(0.5, 0, 1)).toEqual(0.5);
  expect(clamp(-1, 0, 1)).toEqual(0);
  expect(clamp(2, 0, 1)).toEqual(1);
  expect(clamp(1, 2, 1)).toEqual(NaN);
});

/**
 * 将数值限制在指定范围内
 * @param num 输入值
 * @param min 最小值
 * @param max 最大值
 * @returns
 */
export function clamp(num: number, min: number, max: number): number {
  if (min > max) {
    return NaN;
  }
  if (num <= min) {
    return min;
  }
  if (num >= max) {
    return max;
  }
  return num;
}

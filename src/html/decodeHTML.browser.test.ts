/**
 * @vitest-environment jsdom
 */

import { expectRun } from '../test';
import { decodeHTML } from './decodeHTML';

const source = '&quot;&amp;;&lt;&gt;&apos;&nbsp;&#32;abc&34';
const target = '"&;<>\'\u00a0 abc&34';

expectRun({
  name: 'decodeHTML',
  handle: decodeHTML,
  cases: [
    {
      name: 'XML 实体',
      source,
      target,
    },
    {
      name: 'null',
      source: null,
      target: '',
    },
  ],
});

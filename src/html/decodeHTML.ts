import { createElement } from '../dom';
import { toString } from '../object';

/** XML 实体 */
const XML_ENTITY = {
  '&amp;': '&',
  '&lt;': '<',
  '&gt;': '>',
  '&quot;': '"',
  '&apos;': "'",
  '&nbsp;': ' ',
};
const map: Record<string, string> = {};

/**
 * HTML 解码
 *
 * @param html 编码的字符串
 * @returns 解码后的字符串
 */
export function decodeHTML(html: string): string {
  html = toString(html);
  if (!html) {
    return '';
  }
  if (map[html]) {
    return map[html];
  }
  let target: string = html;
  if (typeof document === 'undefined') {
    // Node 环境使用
    target = html.replace(/&(?:amp|lt|gt|quot|apos|nbsp);|&#(\d+);/g, (text: string, code: string): string => {
      if (code) {
        return String.fromCharCode(parseInt(code));
      }
      return XML_ENTITY[text] || text;
    });
  } else {
    // 浏览器环境使用
    const div: HTMLElement = createElement('div');
    div.innerHTML = html;
    target = div.textContent;
  }
  map[html] = target;
  return target;
}

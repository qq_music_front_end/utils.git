import { expectRun } from '../test';
import { decodeHTML } from './decodeHTML';

const source = '&quot;&amp;;&lt;&gt;&apos;&nbsp;&aaaa;&#32;abc&34';
const target = '"&;<>\' &aaaa; abc&34';

expectRun({
  name: 'decodeHTML',
  handle: decodeHTML,
  cases: [
    {
      name: 'XML 实体',
      source,
      target,
    },
    {
      name: '重复用例',
      source,
      target,
    },
    {
      name: 'null',
      source: null,
      target: '',
    },
  ],
});

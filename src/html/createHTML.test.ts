import { expectRun } from '../test';
import { createHTML } from './createHTML';

expectRun({
  name: 'createHTML',
  handle([source, ...sources]: any[]): any {
    return createHTML(source, ...sources);
  },
  cases: [
    {
      name: '带子元素',
      source: [
        'div',
        {
          className: ['wrap', 'head'],
          id: 'js_wrap',
          style: {
            color: '#f00',
            display: null,
          },
          'data-info': {
            length: 1234,
            title: 'abc',
          },
          'data-title': null,
        },
        'abc',
        [123, 4],
      ],
      target:
        '<div class="wrap head" id="js_wrap" style="color:#f00" data-info="{&quot;length&quot;:1234,&quot;title&quot;:&quot;abc&quot;}">abc1234</div>',
    },
    {
      name: '省略结束标签',
      source: ['br'],
      target: '<br />',
    },
  ],
});

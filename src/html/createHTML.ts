import { isArray, isObject } from '../type';
import { encodeHTML } from './encodeHTML';

function formatAttrValue(name: string, value: any): string {
  if (isArray(value)) {
    return value.join(' ');
  }
  if (!isObject(value)) {
    return value;
  }
  if (name !== 'style') {
    return JSON.stringify(value);
  }
  const styleList: string[] = [];
  for (const key in value) {
    const styleValue: string = value[key];
    if (key && styleValue != null) {
      styleList.push(`${key}:${styleValue}`);
    }
  }
  return styleList.join(';');
}

/**
 * 生成 HTML 文本
 * @param tagName 标签名
 * @param props 节点属性
 * @param children 子元素 html 文本
 */
export function createHTML(tagName: string, props?: Record<string, any>, ...children: (string | string[])[]): string {
  let html: string = `<${tagName}`;
  if (props) {
    for (let key in props) {
      let value: any = props[key];
      if (value == null) {
        continue;
      }
      key = key === 'className' ? 'class' : encodeHTML(key);
      value = encodeHTML(formatAttrValue(key, value));
      html += ` ${key}="${value}"`;
    }
  }
  if (/^br|hr|img|input|link|meta$/i.test(tagName)) {
    return `${html} />`;
  }
  const list: string[] = children.map((item: string | string[]): string => {
    if (isArray(item)) {
      return item.join('');
    }
    return item;
  });
  return `${html}>${list.join('')}</${tagName}>`;
}

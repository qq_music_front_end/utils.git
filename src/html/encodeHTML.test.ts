import { expectRun } from '../test';
import { encodeHTML } from './encodeHTML';

const source = '"&;<>\' abc\n34';
const target = '&quot;&amp;;&lt;&gt;&apos; abc 34';

expectRun({
  name: 'encodeHTML',
  handle: encodeHTML,
  cases: [
    {
      name: 'XML 实体',
      source,
      target,
    },
    {
      name: '自定义规则',
      option: {
        ' ': '&nbsp;',
        '\n': '<br />',
      },
      source,
      target: '&quot;&amp;;&lt;&gt;&apos;&nbsp;abc<br />34',
    },
    {
      name: '无需替换',
      source: 'html',
      target: 'html',
    },
    {
      name: 'null',
      source: null,
      target: '',
    },
  ],
});

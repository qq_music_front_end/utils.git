const DEF_RULER: Record<string, string> = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  "'": '&apos;',
  '"': '&quot;',
  '\n': ' ',
};

/**
 * HTML 编码
 *
 * @param text 需要转换编码的字符串
 * @param rule 自定义替换规则
 * @returns html编码的字符串
 */
export function encodeHTML(text: string, rule?: Record<string, string>): string {
  if (!text) {
    return '';
  }
  const reg = /[\u0000-\u002F\u003A-\u0040\u005B-\u005E\u0060\u007B-\u007F]/g;
  if (!reg.test(text)) {
    return text;
  }
  return text
    .replace(reg, (ch: string): string => {
      if (rule && ch in rule) {
        return rule[ch];
      }
      if (DEF_RULER[ch]) {
        return DEF_RULER[ch];
      }
      return ch;
    })
    .replace(/[\u0000-\u0008\u0011-\u001F]+/g, '');
}

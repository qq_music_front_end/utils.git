import { expectRun } from '../test';
import { PATTERN_DATE, PATTERN_DATETIME, PATTERN_DATETIME_LITE, formatDate, formatTime } from './index';

expectRun({
  name: 'date',
  handle: formatDate,
  cases: [
    {
      name: PATTERN_DATE,
      source: 1687993607236,
      target: '2023-06-29',
    },
    {
      name: PATTERN_DATETIME_LITE,
      option: PATTERN_DATETIME_LITE,
      source: 1687993607236,
      target: '2023-06-29 07:06:47',
    },
    {
      name: PATTERN_DATETIME,
      option: PATTERN_DATETIME,
      source: 1687993607236,
      target: '2023-06-29 07:06:47.236',
    },
    {
      name: '自定义',
      option: 'yy年第q季度周w',
      source: 1687993607236,
      target: '23年第2季度周4',
    },
    {
      name: 'hh:MM:ss',
      handle: formatTime,
      source: -123456,
      target: '-02:03',
    },
    {
      name: 'hh:MM:ss',
      handle: formatTime,
      source: 12345678,
      target: '03:26:46',
    },
    {
      name: '无格式化规则',
      option: '',
      source: new Date(1687993607236),
      target: '',
    },
    {
      name: '无效输入',
      source: 'abc',
      target: '',
    },
  ],
});

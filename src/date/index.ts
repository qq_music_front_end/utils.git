/**
 * @file 日期
 */

/** 默认格式化类型 - 日期 */
export const PATTERN_DATE = 'yyyy-mm-dd';

/** 格式化类型 - 日期+时间（秒） */
export const PATTERN_DATETIME_LITE = 'yyyy-mm-dd hh:MM:ss';

/** 格式化类型 - 日期+时间（毫秒） */
export const PATTERN_DATETIME = 'yyyy-mm-dd hh:MM:ss.SSS';

/** 每小时所含毫秒数 */
const hour = 3600000;

/** 每分钟所含毫秒数 */
const minute = 60000;

/** 每秒所含毫秒数 */
const second = 1000;

/** 补齐十位数 */
function pad2(num: number): string {
  return num.toFixed(0).padStart(2, '0');
}

/**
 * 获取日期值
 * @param date 日期
 * @param type 类型
 */
function getDateValue(date: Date, type?: string): number {
  switch (type) {
    // 年
    case 'y':
      return date.getFullYear();
    // 月
    case 'm':
      return date.getMonth() + 1;
    // 日
    case 'd':
      return date.getDate();
    // 小时
    case 'h':
      return date.getHours();
    // 分钟
    case 'M':
      return date.getMinutes();
    // 秒
    case 's':
      return date.getSeconds();
    // 毫秒
    case 'S':
      return date.getMilliseconds();
    // 周几
    case 'w':
      return date.getDay();
    // 第几季度
    case 'q':
      return Math.floor((date.getMonth() + 3) / 3);
  }
}

/**
 * 格式化日期字符串
 * @param date 日期对象
 * @param pattern 格式化规则
 *  - y: 年
 *  - m: 月
 *  - d: 日
 *  - h: 小时
 *  - M: 分钟
 *  - s: 秒
 *  - S: 毫秒
 *  - w: 周几
 *  - q: 第几季度
 * @returns 格式化后的字符串
 */
export function formatDate(date: Date | number | string, pattern: string = PATTERN_DATE): string {
  if (date == null || !pattern) {
    return '';
  }
  const dataObj = new Date(date);
  if (isNaN(dataObj.valueOf?.())) {
    return '';
  }
  return pattern.replace(/(y+|m+|d+|h+|M+|s+|S+|w+|q+)/g, (str: string): string => {
    const value: string = '' + getDateValue(dataObj, str.charAt(0));
    const { length } = value;
    const { length: size } = str;
    if (length === size) {
      return value;
    }
    if (length > size) {
      return value.substring(length - size);
    }
    return value.padStart(size, '0');
  });
}

/**
 * 格式化时间字符串
 * @param ms 毫秒数
 * @returns 时间字符串
 */
export function formatTime(ms: number): string {
  const time: number = ms < 0 ? -ms : ms;
  const h: string = time > hour ? `${pad2(time / hour)}:` : '';
  const m: string = pad2((time % hour) / minute);
  const s: string = pad2((time % minute) / second);
  return `${ms < 0 ? '-' : ''}${h}${m}:${s}`;
}

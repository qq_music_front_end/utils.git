/**
 * 将对象转为字符串
 * @param value 需要转换的值
 * @param type 数值对象要格式化的单位类型
 * - number 用英文逗号分隔的字符串
 * - count 使用`万`|`亿`|`兆`|`京`单位，保留两位小数
 * - size 使用`K`|`M`|`G`|`T`单位，保留两位小数
 * @returns 格式化后的字符串
 * @example
 * toString(123);
 * // => '123'
 *
 * toString(null);
 * // => ''
 *
 * toString(324243244, 'number');
 * // => '324,243,244'
 *
 * toString(324243244, 'count');
 * // => '3.24亿'
 *
 * toString(324243244, 'size');
 * // => '309.22M'
 */
export function toString(value: any, type?: 'number' | 'count' | 'size'): string {
  if (type) {
    if (!value || isNaN(value)) {
      value = 0;
    }
    if (type === 'number') {
      return ('' + value).replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g, '$1,');
    }
    let dev: number;
    let units: string[];
    if (type === 'size') {
      dev = 1024;
      units = ['', 'K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y'];
    } else {
      dev = 10000;
      units = ['', '万', '亿', '兆', '京', '垓'];
    }
    while (value >= dev && units.length > 1) {
      value /= dev;
      units.shift();
    }
    return Math.round(value * 100) / 100 + units[0];
  }
  if (value == null) {
    return '';
  }
  return '' + value;
}

import { getRandomItem } from './getRandomItem';

/**
 * 生成随机字符串
 * @param limit 字符串长度，默认为 4
 * @param chars 可选字符集，默认为 [0-9a-z]
 * @returns 随机字符串
 */
export function createRandomText(limit = 4, chars?: string[]): string {
  const list: string[] = [];
  for (let index: number = 0; index < limit; index++) {
    if (chars) {
      list.push(getRandomItem(chars));
    } else {
      list.push(Math.floor(36 * Math.random()).toString(36));
    }
  }
  return list.join('');
}

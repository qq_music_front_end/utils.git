import { expectRun } from '../test';
import { parseNumUnit } from './parseNumUnit';

expectRun({
  name: 'parseNumUnit',
  handle(source: number, option: any): string {
    if (option) {
      const { pad, showError } = option;
      return parseNumUnit(source, pad, showError);
    }
    return parseNumUnit(source);
  },
  cases: [
    {
      name: '16',
      source: 16,
      target: '',
    },
    {
      name: '123456',
      option: {
        pad: ' ',
      },
      source: 123456,
      target: ' 十万',
    },
    {
      name: '1e50',
      option: {
        showError: true,
      },
      source: 1e50,
      target: '(⊙﹏⊙)b',
    },
    {
      name: '1e64',
      option: {
        showError: true,
      },
      source: 1e64,
      target: 'orz……',
    },
  ],
});

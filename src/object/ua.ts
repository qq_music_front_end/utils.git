import { isRegExp } from '../type';

export interface UADetail {
  name: string;
  version?: string;
  versionName?: string;
}

interface Rule {
  name: string;
  test: RegExp | ((ua: string) => boolean);
  version?: RegExp;
  versionName?(version: string): string;
}

const UNKNOWN = 'unknown';

const ANDROID_VERSION_NAME = {
  '1.5': 'Cupcake',
  '1.6': 'Donut',
  '2': 'Eclair',
  '2.2': 'Froyo',
  '2.3': 'Gingerbread',
  '3': 'Honeycomb',
  '4': 'Jelly Bean',
  '4.0': 'Ice Cream Sandwich',
  '4.4': 'KitKat',
  '5': 'Lollipop',
  '6': 'Marshmallow',
  '7': 'Nougat',
  '8': 'Oreo',
  '9': 'Pie',
  '10': 'Quince Tart',
  '11': 'Red Velvet Cake',
  '12': 'Snow Cone',
  '13': 'Tiramisu',
};

const WINDOWS_VERSION_NAME = {
  '5.0': '2000',
  '5.1': 'XP',
  '5.2': '2003',
  '6.0': 'Vista',
  '6.1': '7',
  '6.2': '8',
  '6.3': '8.1',
  '10.0': '10',
  '11.0': '11',
};

const MAC_OS_VERSION_NAME = {
  '5': 'Leopard',
  '6': 'Snow Leopard',
  '7': 'Lion',
  '8': 'Mountain Lion',
  '9': 'Mavericks',
  '10': 'Yosemite',
  '11': 'El Capitan',
  '12': 'Sierra',
  '13': 'High Sierra',
  '14': 'Mojave',
  '15': 'Catalina',
};

const RULE: Record<'browser' | 'os' | 'platform' | 'engine', Rule[]> = {
  /** 浏览器 */
  browser: [
    {
      name: 'Opera',
      test: /opera/i,
      version: /opera[\s/](\d+(\.?_?\d+)+)/i,
    },
    {
      name: 'Internet Explorer',
      test: /msie|trident/i,
      version: /(?:msie |rv:)(\d+(\.?_?\d+)+)/i,
    },
    {
      name: 'Microsoft Edge',
      test: /Edg([ea]|ios)/i,
      version: /Edg([ea]|ios)\/(\d+(\.?_?\d+)+)/i,
    },
    {
      name: 'Firefox',
      test: /firefox|iceweasel|fxios/i,
      version: /(?:firefox|iceweasel|fxios)[\s/](\d+(\.?_?\d+)+)/i,
    },
    {
      name: 'Chromium',
      test: /chromium/i,
      version: /chromium[\s/](\d+(\.?_?\d+)+)/i,
    },
    {
      name: 'Chrome',
      test: /chrome|crios|crmo/i,
      version: /(?:chrome|crios|crmo)\/(\d+(\.?_?\d+)+)/i,
    },
    {
      name: 'Android Browser',
      test: /Android/,
      version: /version\/(\d+(\.?_?\d+)+)/i,
    },
    {
      name: 'Safari',
      test: /Safari|AppleWebKit/i,
      version: /Safari\/(\d+(\.?_?\d+)+)/i,
    },
  ],
  /** 引擎 */
  engine: [
    {
      name: 'EdgeHTML',
      test: /\sEdge\//i,
      version: /Edge\/(\d+(\.?_?\d+)+)/i,
    },
    {
      name: 'Trident',
      test: /trident/i,
      version: /trident\/(\d+(\.?_?\d+)+)/i,
    },
    {
      name: 'Presto',
      test: /presto/i,
      version: /presto\/(\d+(\.?_?\d+)+)/i,
    },
    {
      name: 'Gecko',
      test(ua: string): boolean {
        return /gecko/i.test(ua) && !/like gecko/i.test(ua);
      },
      version: /gecko\/(\d+(\.?_?\d+)+)/i,
    },
    {
      name: 'Blink',
      test: /(?:apple)?webkit\/537\.36/i,
    },
    {
      name: 'WebKit',
      test: /(?:apple)?webkit/i,
      version: /webkit\/(\d+(\.?_?\d+)+)/i,
    },
  ],
  /** 系统 */
  os: [
    {
      name: 'Windows Phone',
      test: /Windows Phone/i,
      version: /Windows Phone (?:os)?\s?(\d+(\.\d+)*)/i,
    },
    {
      name: 'Windows',
      test: /Windows /i,
      version: /Windows NT (\d+.\d)?/i,
      versionName(version: string): string {
        return WINDOWS_VERSION_NAME[version];
      },
    },
    {
      name: 'Android',
      test: /Android/i,
      version: /Android[\s/-](\d+(\.\d+)*)/i,
      versionName(version: string): string {
        const [major, strMinor] = version.split('.');
        const minor: number = parseInt(strMinor);
        const detail: string = `${major}.${minor}`;
        return ANDROID_VERSION_NAME[detail] || ANDROID_VERSION_NAME[version];
      },
    },
    {
      name: 'Mac OS',
      test: /Macintosh/i,
      version: /Mac OS X (\d+(\.?_?\d+)+)/i,
      versionName(version: string): string {
        const [major, minor] = version.split(/[._]/);
        if (major === '10') {
          return MAC_OS_VERSION_NAME[minor];
        }
      },
    },
    {
      name: 'iOS',
      test: /(iPod|iPhone|iPad)/i,
      version: /OS (\d+([_\s]\d+)*) like Mac OS X/i,
    },
    {
      name: 'Linux',
      test: /Linux/i,
    },
  ],
  /** 应用 */
  platform: [
    {
      name: 'Baiduspider',
      test: /Baiduspider/,
      version: /Baiduspider-render\/(\d+(\.\d+)+)/i,
    },
    {
      name: 'Electron',
      test: /electron/i,
      version: /electron\/(\d+(\.?_?\d+)+)/i,
    },
    {
      name: 'Microsoft Edge',
      test: /\sedg\//i,
      version: /\sedg\/(\d+(\.?_?\d+)+)/i,
    },
    {
      name: 'Huawei Browser',
      test: /HuaweiBrowser/i,
      version: /HuaweiBrowser[\s/](\d+(\.?_?\d+)+)/i,
    },
    {
      name: 'QQMusic PC',
      test: /\spcqqmusic/,
      version: /pcqqmusic\/(\d+(\.\d+)+)/i,
    },
    {
      name: 'QQMusic HD',
      test: /\sHDQQMusic/,
      version: /HDQQMusic\/(\d+(\.\d+)+)/i,
    },
    {
      name: 'QQMusic',
      test: /\sQQMusic/,
      version: /\sQQMusic\/(\d+(\.\d+)+)/i,
    },
    {
      name: 'UC Browser',
      test: /ucbrowser/i,
      version: /ucbrowser[\s/](\d+(\.?_?\d+)+)/i,
    },
    {
      name: 'Maxthon',
      test: /Maxthon|mxios/i,
      version: /(?:Maxthon|mxios)[\s/](\d+(\.?_?\d+)+)/i,
    },
    {
      name: 'Wechat Windows',
      test: /WindowsWechat/i,
    },
    {
      name: 'WeChat',
      test: /MicroMessenger/i,
      version: /\sMicroMessenger[\s/](\d+(\.?_?\d+)+)/i,
    },
    {
      name: 'QQ',
      test: /\sQQ\//i,
      version: /\sQQ\/(\d+(\.?_?\d+)+)/i,
    },
    {
      name: 'QQ Browser',
      test: /\sQQBrowser/i,
      version: /\sQQBrowser[/](\d+(\.?_?\d+)+)/i,
    },
  ],
};

function parse(ua: string, rules: Rule[]): UADetail {
  for (let index: number = 0; index < rules.length; index++) {
    const { name, test, version, versionName } = rules[index];
    if (isRegExp(test)) {
      if (!test.test(ua)) {
        continue;
      }
    } else if (!test(ua)) {
      continue;
    }
    const result: UADetail = { name };
    const ver: string = ua.match(version)?.[1];
    if (ver) {
      result.version = ver.replace(/_/g, '.');
      if (versionName) {
        result.versionName = versionName(ver);
      }
    }
    return result;
  }
  return { name: UNKNOWN };
}

/**
 * UA 解析
 */
export class UA {
  static map = new Map<string, UA>();

  ua: string;
  browser: UADetail;
  engine: UADetail;
  os: UADetail;
  platform: UADetail;

  constructor(ua?: string) {
    if (!ua && typeof navigator != 'undefined') {
      ua = navigator.userAgent;
    }
    this.ua = '' + ua;
    this.parse();
  }

  parse(ua: string = this.ua): void {
    this.ua = ua;
    if (UA.map.has(ua)) {
      const cache: UA = UA.map.get(ua);
      this.browser = Object.assign(this.browser || {}, cache.browser);
      this.engine = Object.assign(this.engine || {}, cache.engine);
      this.os = Object.assign(this.os || {}, cache.os);
      this.platform = Object.assign(this.platform || {}, cache.platform);
    } else {
      this.browser = parse(ua, RULE.browser);
      this.engine = parse(ua, RULE.engine);
      this.os = parse(ua, RULE.os);
      this.platform = parse(ua, RULE.platform);
      if (this.platform.name === UNKNOWN) {
        this.platform = this.browser;
      }
      UA.map.set(ua, this);
    }
  }
}

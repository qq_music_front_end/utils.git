import { expectRun } from '../test';
import { encodeForm } from './encodeForm';

expectRun({
  name: 'encodeForm',
  handle([...sources]: any[]): any {
    return encodeForm(...sources);
  },
  cases: [
    {
      name: '对象',
      source: [
        {
          key1: 'value1',
          key2: [1, 2, null, 4],
          key3: { sub: 1 },
          key4: null,
        },
      ],
      target: {
        key1: 'value1',
        'key2[]': 1,
        'key2[1]': 2,
        'key2[3]': 4,
        'key3[sub]': 1,
        key4: '',
      },
    },
    {
      name: '带前缀',
      source: [
        {
          key1: 'value1',
          key2: [1, 2, 3],
          key3: { sub: 1 },
        },
        'mm',
      ],
      target: {
        'mm[key1]': 'value1',
        'mm[key2][]': 1,
        'mm[key2][1]': 2,
        'mm[key2][2]': 3,
        'mm[key3][sub]': 1,
      },
    },
    {
      name: '带前缀',
      source: [
        {
          key1: 'value1',
          key2: [1, 2, 3],
          key3: { sub: 1 },
        },
        'mm',
      ],
      target: {
        'mm[key1]': 'value1',
        'mm[key2][]': 1,
        'mm[key2][1]': 2,
        'mm[key2][2]': 3,
        'mm[key3][sub]': 1,
      },
    },
  ],
});

export interface ParseOptions {
  type?: 'json' | 'script';
  error?: boolean;
}

/**
 * 解析 JSON 字符串
 * @param obj
 * @returns
 */
export function parseJSON<T = any>(obj: string, options?: ParseOptions): T | undefined {
  if (obj) {
    try {
      return JSON.parse(obj);
    } catch (ex) {
      if (options?.error) {
        throw ex;
      }
    }
  }
}

/**
 * 从字符串中解析对象
 * @param obj
 * @returns
 */
export function parse<T = any>(obj: string, args?: Record<string, any>, options?: ParseOptions): T | undefined {
  if (obj) {
    try {
      const argNameList: string[] = [];
      const argValueList: any[] = [];
      if (args) {
        for (const key in args) {
          argNameList.push(key);
          argValueList.push(args[key]);
        }
      }
      if (options?.type != 'script') {
        obj = `return (${obj})`;
      }
      return new Function(...argNameList, obj)(...argValueList);
    } catch (ex) {
      if (options?.error) {
        throw ex;
      }
    }
  }
}

/**
 * 从字符串中解析 exports 对象
 * @param objStr
 * @param exports
 * @returns
 * @example
 *  parseExports('exports.config = { key: "value" }', 'exports')
 *  // => { config: { key: 'value' } }
 */
export function parseExports<T = any>(objStr: any, exports = 'exports'): T | void {
  const exp: Record<string, any> = {};
  parse(
    objStr,
    {
      [exports]: exp,
    },
    {
      type: 'script',
    },
  );
  return exp as T;
}

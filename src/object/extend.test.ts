import { expectRun } from '../test';
import { extend } from './extend';

const same = { a: 1 };

expectRun({
  name: 'extend',
  handle([source, ...sources]: any[]): any {
    return extend(source, ...sources);
  },
  cases: [
    {
      name: '对象',
      source: [
        {
          key: 1234,
          list: [4, 3, 2, 1],
          obj: {},
          obj2: 1,
        },
        null,
        {
          list: [1, 2, 3],
          obj: {
            str: 'string',
          },
          obj2: {
            str: 'string',
          },
        },
      ],
      target: {
        key: 1234,
        list: [1, 2, 3, 1],
        obj: {
          str: 'string',
        },
        obj2: {
          str: 'string',
        },
      },
    },
    {
      name: '只有第一个参数',
      source: [1234],
      target: 1234,
    },
    {
      name: '第一个参数为 null',
      source: [
        null,
        {
          list: [1, 2, 3],
        },
      ],
      target: {
        list: [1, 2, 3],
      },
    },
    {
      name: '相同对象',
      source: [{}, same, same],
      target: same,
    },
  ],
});

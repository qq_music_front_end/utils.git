import { expect } from 'vitest';
import { expectRun } from '../test';
import { parse, parseExports, parseJSON } from './parse';

const target = { config: { key: 'value' } };

expectRun({
  name: 'parse',
  cases: [
    {
      name: 'parse',
      handle: parse,
      source: '{ config: { key: "value" }}',
      target,
    },
    {
      name: 'parse 空数据',
      handle: parse,
      source: '',
      target: undefined,
    },
    {
      name: 'parse 无效数据',
      handle: parse,
      source: '{ config: { key: "value" }',
      target: undefined,
    },
    {
      name: 'parse 异常抛出',
      handle(source: string): any {
        expect((): any => parse(source, {}, { error: true })).toThrowError('Unexpected token');
      },
      source: '{ config: { key: "value" }',
      target: undefined,
    },
    {
      name: 'parseJSON',
      handle: parseJSON,
      source: '{ "config": { "key": "value" }}',
      target,
    },
    {
      name: 'parseJSON 空数据',
      handle: parseJSON,
      source: '',
      target: undefined,
    },
    {
      name: 'parseJSON 无效数据',
      handle: parseJSON,
      source: '{ config: { "key": "value" }}',
      target: undefined,
    },
    {
      name: 'parseJSON 异常抛出',
      handle(source: string): any {
        expect((): any => parseJSON(source, { error: true })).toThrowError('Unexpected token');
      },
      source: '{ config: { "key": "value" }}',
      target: undefined,
    },
    {
      name: 'parseExports',
      handle: parseExports,
      source: 'exports.config = { key: "value" }',
      target,
    },
    {
      name: 'parseExports - 自定义 exports 参数',
      handle: parseExports,
      option: 'win',
      source: 'win.config = { key: "value" }',
      target,
    },
  ],
});

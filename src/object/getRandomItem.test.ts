import { expect, test } from 'vitest';
import { getRandomItem } from './getRandomItem';

test('getRandomItem', (): void => {
  const source: number[] = [1, 2, 3, 4];
  const item: number = getRandomItem(source);
  expect(source.includes(item)).toEqual(true);
});

test('getRandomItem-not-array', (): void => {
  const source = 'not-array';
  const item: any = getRandomItem(source as any);
  expect(item).toEqual(source);
});

import { isArray } from '../type';

/**
 * 随机获取数组中的一个元素
 * @param list
 * @returns
 */
export function getRandomItem<T = any>(list: T[]): T {
  if (!isArray(list)) {
    return list;
  }
  if (list.length === 1) {
    return list[0];
  }
  return list[Math.floor(Math.random() * list.length)];
}

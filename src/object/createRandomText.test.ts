import { expect, test } from 'vitest';
import { createRandomText } from './createRandomText';

test('createRandomText', (): void => {
  const text1: string = createRandomText();
  const text2: string = createRandomText();
  const text3: string = createRandomText(3, ['*']);
  expect(text1 === text2).toBe(false);
  expect(text2.length).toBe(4);
  expect(text1.length).toBe(4);
  expect(text3).toBe('***');
});

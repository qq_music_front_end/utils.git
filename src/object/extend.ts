import { isArray, isObject } from '../type';

/**
 * 扩展对象
 * @param target 目标对象
 * @param sources 源对象
 * @returns 目标对象
 */
export function extend<T>(target: any, ...sources: any[]): T {
  if (target == null) {
    target = {};
  }
  const isArr: boolean = isArray(target);
  const isObj: boolean = !isArr && isObject(target);
  if ((!isArr && !isObj) || !sources.length) {
    return target;
  }
  sources.forEach((source: any): void => {
    if (!source || target === source) {
      return;
    }
    for (const name in source) {
      const curValue: any = target[name];
      const newValue: any = source[name];
      if (curValue && curValue === newValue) {
        continue;
      }
      if (isObject(newValue)) {
        target[name] = extend(curValue && isObject(curValue) ? curValue : {}, newValue);
      } else if (isArray(newValue)) {
        target[name] = extend(isArray(curValue) ? curValue : [], newValue);
      } else {
        target[name] = newValue;
      }
    }
  });
  return target;
}

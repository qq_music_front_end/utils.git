import { expectRun } from '../test';
import { toString } from './toString';

expectRun({
  name: 'toString',
  handle: toString,
  cases: [
    {
      name: '数值',
      source: 3.1415,
      target: '3.1415',
    },
    {
      name: 'null',
      source: null,
      target: '',
    },
    {
      name: '0',
      source: 0,
      target: '0',
      option: 'number',
    },
    {
      name: 'number',
      source: 324243244,
      target: '324,243,244',
      option: 'number',
    },
    {
      name: 'count',
      source: 324243244,
      target: '3.24亿',
      option: 'count',
    },
    {
      name: 'size',
      source: 324243244,
      target: '309.22M',
      option: 'size',
    },
  ],
});

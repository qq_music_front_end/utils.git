import { expectRun } from '../test';
import { UA } from './ua';

(global.navigator as any) = {
  userAgent: 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/113.0',
};

expectRun({
  name: 'UA',
  handle(source: string): any {
    const { browser, engine, os, platform } = new UA(source);
    return { browser, engine, os, platform };
  },
  cases: [
    {
      name: 'Windows 10 / Firefox',
      source: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/112.0',
      target: {
        browser: { name: 'Firefox', version: '112.0' },
        engine: { name: 'Gecko', version: '20100101' },
        os: { name: 'Windows', version: '10.0', versionName: '10' },
        platform: { name: 'Firefox', version: '112.0' },
      },
    },
    {
      name: 'Windows 8.1 / QQ音乐',
      source:
        'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.47.134 Safari/537.36 QBCore/3.53.47.400 QQBrowser/9.0.2524.400 pcqqmusic/19.44.2235.0731 SkinId/10001|1ecc94|144|1|||1fd4af',
      target: {
        browser: { name: 'Chrome', version: '53.0.47.134' },
        engine: { name: 'Blink' },
        os: { name: 'Windows', version: '6.3', versionName: '8.1' },
        platform: { name: 'QQMusic PC', version: '19.44.2235.0731' },
      },
    },
    {
      name: 'Windows 7 / IE 9',
      source: 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0',
      target: {
        browser: { name: 'Internet Explorer', version: '9.0' },
        engine: { name: 'Trident', version: '5.0' },
        os: { name: 'Windows', version: '6.1', versionName: '7' },
        platform: { name: 'Internet Explorer', version: '9.0' },
      },
    },
    {
      name: 'Windows XP / IE 6',
      source: 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)',
      target: {
        browser: { name: 'Internet Explorer', version: '6.0' },
        engine: { name: 'unknown' },
        os: { name: 'Windows', version: '5.1', versionName: 'XP' },
        platform: { name: 'Internet Explorer', version: '6.0' },
      },
    },
    {
      name: 'Windows Phone',
      source:
        'Mozilla/5.0 (Mobile; Windows Phone 8.1; Android 4.0; ARM; Trident/7.0; Touch; rv:11.0; IEMobile/11.0; NOKIA; Nokia 920T) like iPhone OS 7_0_3 Mac OS X AppleWebKit/537 (KHTML, like Gecko) Mobile Safari/537',
      target: {
        browser: { name: 'Internet Explorer', version: '11.0' },
        engine: { name: 'Trident', version: '7.0' },
        os: { name: 'Windows Phone', version: '8.1' },
        platform: { name: 'Internet Explorer', version: '11.0' },
      },
    },
    {
      name: 'Android 10 / HuaweiBrowser',
      source:
        'Mozilla/5.0 (Linux; Android 10; HarmonyOS; LYA-AL10; HMSCore 6.7.0.322; GMSCore 20.15.16) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.105 HuaweiBrowser/12.1.3.303 Mobile Safari/537.36',
      target: {
        browser: { name: 'Chrome', version: '92.0.4515.105' },
        engine: { name: 'Blink' },
        os: { name: 'Android', version: '10', versionName: 'Quince Tart' },
        platform: { name: 'Huawei Browser', version: '12.1.3.303' },
      },
    },
    {
      name: 'iPhone / QQ音乐',
      source:
        'Mozilla/5.0 (iPhone; CPU iPhone OS 15_7_5 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Mobile/12A365 QQMusic/12.3.0 Mskin/white Mcolor/3de58cff Bcolor/00000000 skinid[902] NetType/4G WebView/WKWebView Released[1] zh-CN DeviceModel/iPhone8,2 skin_css/skin2_1_902 Pixel/1242 FreeFlow/0 teenMode/0 nft_released/[1] ui_mode/1 FontMode/0] H5/1',
      target: {
        browser: { name: 'Safari' },
        engine: { name: 'WebKit', version: '600.1.4' },
        os: { name: 'iOS', version: '15.7.5' },
        platform: { name: 'QQMusic', version: '12.3.0' },
      },
    },
    {
      name: 'Mac OS X',
      source:
        'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_8; en-us) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1 Safari/534.50',
      target: {
        browser: { name: 'Safari', version: '534.50' },
        engine: { name: 'WebKit', version: '534.50' },
        os: { name: 'Mac OS', version: '10.6.8', versionName: 'Snow Leopard' },
        platform: { name: 'Safari', version: '534.50' },
      },
    },
    {
      name: 'Mac OS X / QQ音乐',
      source:
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 13.3.0 ) AppleWebKit/600.1.4 (KHTML, like Gecko) patch/2 QQMusic/8.8.1 Released[1] Skinid/10209|||2|||1fd4af',
      target: {
        browser: { name: 'Safari' },
        engine: { name: 'WebKit', version: '600.1.4' },
        os: { name: 'Mac OS', version: '13.3.0' },
        platform: { name: 'QQMusic', version: '8.8.1' },
      },
    },
    {
      name: 'Linux / Firefox',
      source: 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/113.0',
      target: {
        browser: { name: 'Firefox', version: '113.0' },
        engine: { name: 'Gecko', version: '20100101' },
        os: { name: 'Linux' },
        platform: { name: 'Firefox', version: '113.0' },
      },
    },
    {
      name: '系统UA',
      source: '',
      target: {
        browser: { name: 'Firefox', version: '113.0' },
        engine: { name: 'Gecko', version: '20100101' },
        os: { name: 'Linux' },
        platform: { name: 'Firefox', version: '113.0' },
      },
    },
  ],
});

import { isArray, isFormData, isObject, isTrueEmpty } from '../type';

function encode(result: Record<string, any>, obj: any, prefix: string, form?: FormData): Record<string, any> {
  if (isObject(obj)) {
    for (const key in obj) {
      encode(result, obj[key], prefix ? `${prefix}[${key}]` : key, form);
    }
  } else if (isArray(obj)) {
    obj.forEach((item: any, index: number): void => {
      if (!isTrueEmpty(item)) {
        encode(result, item, `${prefix}[${index > 0 ? index : ''}]`, form);
      }
    });
  } else {
    if (obj == null) {
      obj = '';
    }
    result[prefix] = obj;
    if (form) {
      form.append(prefix, obj);
    }
  }
  return result;
}

/**
 * 普通对象转为表单对象
 * @param obj 要转换的对象
 * @param prefix 路径前缀
 * @param form 要保存到的FormData对象
 * @example
 *  encodeForm({ key1: 'value1', key2: [1, 2, 3], key3: { sub: 1 } })
 *  // => {"key1":"value1","key2[]":1,"key2[1]":2,"key2[2]":3,"key3[sub]":1}
 */
export function encodeForm(
  obj?: Record<string, any>,
  prefix?: string | FormData,
  form?: FormData,
): Record<string, any> {
  if (isFormData(prefix)) {
    form = prefix;
    prefix = '';
  } else if (!isFormData(form)) {
    form = undefined;
  }
  return encode({}, obj, prefix || '', form);
}

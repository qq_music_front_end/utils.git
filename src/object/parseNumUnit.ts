/**
 * 解析计数单位，支持十进制48位
 * @param num 要计算的数
 * @param pad 前缀字符串
 * @param showError 超过限制时是否展示错误提示
 * @returns 计数单位
 */
export function parseNumUnit(num: number, pad = '', showError = false): string {
  if (num >= 100) {
    num = Math.floor(Math.log10(num));
    const a: number = num % 4;
    const b: number = Math.floor(num / 4);
    const unitA: string[] = ['', '十', '百', '千'];
    const unitB: string[] = ['', '万', '亿', '兆', '京', '垓', '秭', '穰', '沟', '涧', '正', '载'];
    if (b < unitB.length) {
      return pad + unitA[a] + unitB[b];
    }
    if (showError) {
      const errorLevel: string[] = [
        '(⊙﹏⊙)b', // 1e48
        '还有(◎_◎;)', // 1e52
        '不会吧ヾ(ﾟ∀ﾟゞ)', // 1e56
        '!!!∑(ﾟДﾟノ)ノ', // 1e60
      ];
      const n: number = b - unitB.length;
      return errorLevel[n] || 'orz……';
    }
  }
  return '';
}

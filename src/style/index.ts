/**
 * @file 颜色样式
 */

function tag(code: number) {
  return `\x1b[${code}m`;
}

/**
 * 生成带样式的控制台文本
 * @param str 原文本
 * @param open 开始标记
 * @param close 结束标记
 * @returns
 */
function format(str: any, open: number, close: number): string {
  const openTag: string = tag(open);
  const closeTag: string = tag(close);
  const closeReg = new RegExp(`\\x1b\\[${close}m`, 'g');
  return `${openTag}${('' + str).replace(closeReg, openTag)}${closeTag}`;
}

/** 重置样式 */
export function reset(str: any): string {
  return format(str, 0, 0);
}

/** 加粗 */
export function bold(str: any): string {
  return format(str, 1, 22);
}

/** 斜体 */
export function italic(str: any): string {
  return format(str, 3, 23);
}

/** 下划线 */
export function underline(str: any): string {
  return format(str, 4, 24);
}

/** 反转前景色和背景色 */
export function inverse(str: any): string {
  return format(str, 7, 27);
}

/** 隐藏 */
export function hidden(str: any): string {
  return format(str, 8, 28);
}

/** 删除线 */
export function strikethrough(str: any): string {
  return format(str, 9, 29);
}

/** 前景色-白色 */
export function white(str: any): string {
  return format(str, 37, 39);
}

/** 前景色-黑色 */
export function black(str: any): string {
  return format(str, 30, 39);
}

/** 前景色-红色 */
export function red(str: any): string {
  return format(str, 31, 39);
}

/** 前景色-绿色 */
export function green(str: any): string {
  return format(str, 32, 39);
}

/** 前景色-黄色 */
export function yellow(str: any): string {
  return format(str, 33, 39);
}

/** 前景色-蓝色 */
export function blue(str: any): string {
  return format(str, 34, 39);
}

/** 前景色-洋红色 */
export function magenta(str: any): string {
  return format(str, 35, 39);
}

/** 前景色-青色 */
export function cyan(str: any): string {
  return format(str, 36, 39);
}

/** 前景色-灰色 */
export function gray(str: any): string {
  return format(str, 90, 39);
}

/** 前景色-亮红色 */
export function brightRed(str: any): string {
  return format(str, 91, 39);
}

/** 前景色-亮绿色 */
export function brightGreen(str: any): string {
  return format(str, 92, 39);
}

/** 前景色-亮黄色 */
export function brightYellow(str: any): string {
  return format(str, 93, 39);
}

/** 前景色-亮蓝色 */
export function brightBlue(str: any): string {
  return format(str, 94, 39);
}

/** 前景色-淡红色 */
export function brightMagenta(str: any): string {
  return format(str, 95, 39);
}

/** 前景色-亮青色 */
export function brightCyan(str: any): string {
  return format(str, 96, 39);
}

/** 背景色-黑色 */
export function bgBlack(str: any): string {
  return format(str, 40, 49);
}

/** 背景色-红色 */
export function bgRed(str: any): string {
  return format(str, 41, 49);
}

/** 背景色-绿色 */
export function bgGreen(str: any): string {
  return format(str, 42, 49);
}

/** 背景色-黄色 */
export function bgYellow(str: any): string {
  return format(str, 43, 49);
}

/** 背景色-蓝色 */
export function bgBlue(str: any): string {
  return format(str, 44, 49);
}

/** 背景色-洋红色 */
export function bgMagenta(str: any): string {
  return format(str, 45, 49);
}

/** 背景色-青色 */
export function bgCyan(str: any): string {
  return format(str, 46, 49);
}

/** 背景色-白色 */
export function bgWhite(str: any): string {
  return format(str, 47, 49);
}

/** 背景色-亮红色 */
export function bgBrightRed(str: any): string {
  return format(str, 101, 49);
}

/** 背景色-亮绿色 */
export function bgBrightGreen(str: any): string {
  return format(str, 102, 49);
}

/** 背景色-亮黄色 */
export function bgBrightYellow(str: any): string {
  return format(str, 103, 49);
}

/** 背景色-亮蓝色 */
export function bgBrightBlue(str: any): string {
  return format(str, 104, 49);
}

/** 背景色-淡红色 */
export function bgBrightMagenta(str: any): string {
  return format(str, 105, 49);
}

/** 背景色-亮青色 */
export function bgBrightCyan(str: any): string {
  return format(str, 106, 49);
}

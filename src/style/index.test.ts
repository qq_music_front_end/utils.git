import { expectRun } from '../test';
import {
  bgBlack,
  bgBlue,
  bgBrightBlue,
  bgBrightCyan,
  bgBrightGreen,
  bgBrightMagenta,
  bgBrightRed,
  bgBrightYellow,
  bgCyan,
  bgGreen,
  bgMagenta,
  bgRed,
  bgWhite,
  bgYellow,
  black,
  blue,
  bold,
  brightBlue,
  brightCyan,
  brightGreen,
  brightMagenta,
  brightRed,
  brightYellow,
  cyan,
  gray,
  green,
  hidden,
  inverse,
  italic,
  magenta,
  red,
  reset,
  strikethrough,
  underline,
  white,
  yellow,
} from './index';

const source = 'source';

expectRun({
  name: 'parse',
  source,
  cases: [
    {
      name: 'reset',
      handle: reset,
      target: `\x1b[0m${source}\x1b[0m`,
    },
    {
      name: 'bold',
      handle: bold,
      target: `\x1b[1m${source}\x1b[22m`,
    },
    {
      name: 'italic',
      handle: italic,
      target: `\x1b[3m${source}\x1b[23m`,
    },
    {
      name: 'underline',
      handle: underline,
      target: `\x1b[4m${source}\x1b[24m`,
    },
    {
      name: 'inverse',
      handle: inverse,
      target: `\x1b[7m${source}\x1b[27m`,
    },
    {
      name: 'hidden',
      handle: hidden,
      target: `\x1b[8m${source}\x1b[28m`,
    },
    {
      name: 'strikethrough',
      handle: strikethrough,
      target: `\x1b[9m${source}\x1b[29m`,
    },
    {
      name: 'white',
      handle: white,
      target: `\x1b[37m${source}\x1b[39m`,
    },
    {
      name: 'black',
      handle: black,
      target: `\x1b[30m${source}\x1b[39m`,
    },
    {
      name: 'red',
      handle: red,
      target: `\x1b[31m${source}\x1b[39m`,
    },
    {
      name: 'green',
      handle: green,
      target: `\x1b[32m${source}\x1b[39m`,
    },
    {
      name: 'yellow',
      handle: yellow,
      target: `\x1b[33m${source}\x1b[39m`,
    },
    {
      name: 'blue',
      handle: blue,
      target: `\x1b[34m${source}\x1b[39m`,
    },
    {
      name: 'magenta',
      handle: magenta,
      target: `\x1b[35m${source}\x1b[39m`,
    },
    {
      name: 'cyan',
      handle: cyan,
      target: `\x1b[36m${source}\x1b[39m`,
    },
    {
      name: 'gray',
      handle: gray,
      target: `\x1b[90m${source}\x1b[39m`,
    },
    {
      name: 'brightRed',
      handle: brightRed,
      target: `\x1b[91m${source}\x1b[39m`,
    },
    {
      name: 'brightGreen',
      handle: brightGreen,
      target: `\x1b[92m${source}\x1b[39m`,
    },
    {
      name: 'brightYellow',
      handle: brightYellow,
      target: `\x1b[93m${source}\x1b[39m`,
    },
    {
      name: 'brightBlue',
      handle: brightBlue,
      target: `\x1b[94m${source}\x1b[39m`,
    },
    {
      name: 'brightMagenta',
      handle: brightMagenta,
      target: `\x1b[95m${source}\x1b[39m`,
    },
    {
      name: 'brightCyan',
      handle: brightCyan,
      target: `\x1b[96m${source}\x1b[39m`,
    },
    {
      name: 'bgBlack',
      handle: bgBlack,
      target: `\x1b[40m${source}\x1b[49m`,
    },
    {
      name: 'bgRed',
      handle: bgRed,
      target: `\x1b[41m${source}\x1b[49m`,
    },
    {
      name: 'bgGreen',
      handle: bgGreen,
      target: `\x1b[42m${source}\x1b[49m`,
    },
    {
      name: 'bgYellow',
      handle: bgYellow,
      target: `\x1b[43m${source}\x1b[49m`,
    },
    {
      name: 'bgBlue',
      handle: bgBlue,
      target: `\x1b[44m${source}\x1b[49m`,
    },
    {
      name: 'bgMagenta',
      handle: bgMagenta,
      target: `\x1b[45m${source}\x1b[49m`,
    },
    {
      name: 'bgCyan',
      handle: bgCyan,
      target: `\x1b[46m${source}\x1b[49m`,
    },
    {
      name: 'bgWhite',
      handle: bgWhite,
      target: `\x1b[47m${source}\x1b[49m`,
    },
    {
      name: 'bgBrightRed',
      handle: bgBrightRed,
      target: `\x1b[101m${source}\x1b[49m`,
    },
    {
      name: 'bgBrightGreen',
      handle: bgBrightGreen,
      target: `\x1b[102m${source}\x1b[49m`,
    },
    {
      name: 'bgBrightYellow',
      handle: bgBrightYellow,
      target: `\x1b[103m${source}\x1b[49m`,
    },
    {
      name: 'bgBrightBlue',
      handle: bgBrightBlue,
      target: `\x1b[104m${source}\x1b[49m`,
    },
    {
      name: 'bgBrightMagenta',
      handle: bgBrightMagenta,
      target: `\x1b[105m${source}\x1b[49m`,
    },
    {
      name: 'bgBrightCyan',
      handle: bgBrightCyan,
      target: `\x1b[106m${source}\x1b[49m`,
    },
  ],
});

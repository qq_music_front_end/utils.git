import { describe, expect, test } from 'vitest';

interface TestHandle<T = any, S = any, O = any> {
  (source: T, option: O): S;
}

export type TestItem<T = any, S = any, O = any> = {
  handle?: TestHandle;
  name: string;
  source?: T;
  target?: S;
  option?: O;
};

export interface TestTask<T = any, S = any, O = any> {
  cases: TestItem<T, S, O>[];
  handle?: TestHandle;
  name: string;
  source?: T;
  target?: any;
}

export function expectRun({ name, cases, source: groupSource, handle: groupHandle }: TestTask): void {
  describe.concurrent(name, (): void => {
    cases.forEach((item: TestItem): void => {
      const { handle: itemHandle, name, option, source, target } = item;
      test(name, async (): Promise<void> => {
        const handle: TestHandle = itemHandle || groupHandle;
        const result: any = await handle('source' in item ? source : groupSource, option);
        expect(result).toEqual(target);
      });
    });
  });
}

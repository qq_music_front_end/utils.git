import { sleep } from '../flow';
import { getJSONP } from './getJSONP';

export interface UajaxInit {
  /** 服务地址 */
  url?: string;
  /** 公共参数 */
  comm?: Record<string, any>;
}

export interface UajaxReqInit {
  /** 请求模块 */
  module: string;
  /** 请求方法 */
  method: string;
  /** 请求参数 */
  param?: Record<string, any>;
}

export interface UajaxRes<T> {
  code?: number;
  ts?: number;
  start_ts?: number;
  traceid?: string;
  data?: T;
}

export interface Uajax {
  request<T = any>(data: UajaxReqInit): Promise<UajaxRes<T>>;
}

/**
 * 发送统一 CGI 请求
 * @param init
 * @example
 *  const req = uajax();
 *  const result = await req.request({
 *    module: 'MvService.MvInfoProServer',
 *    method: 'GetMvInfoList',
 *    param: { vidlist: ['p0017ku7hzh'] }
 *  });
 *  // 并行请求
 *  const results = await Promise.all([
 *      req.request({ ... }),
 *      req.request({ ... }),
 *  ]);
 */
export function uajax(init?: UajaxInit): Uajax {
  const { url, comm } = init || {};
  let wait: Promise<any> | null;
  let reqData: Record<string, any>;
  let index: number = 0;

  /** 发送请求 */
  async function sendRequest(): Promise<any> {
    // 利用异步逻辑合并请求
    await sleep();
    // 清除等待标记，下次再有调用时会发起一次新的请求
    wait = null;
    const result: any = await getJSONP({
      url: url || 'https://u.y.qq.com/cgi-bin/musicu.fcg',
      data: {
        format: 'json',
        data: JSON.stringify(reqData),
        _: Date.now(),
      },
    });
    if (result?.code !== 0) {
      throw result;
    }
    return result;
  }

  return {
    /**
     * 发起请求
     * @param data 请求设置
     */
    async request<T = any>(data: UajaxReqInit): Promise<UajaxRes<T>> {
      if (!data || !data.module || !data.method) {
        return {};
      }
      if (!wait) {
        reqData = { comm };
        wait = sendRequest();
      }
      const key = `req_${++index}`;
      if (!data.param) {
        data.param = {};
      }
      reqData[key] = data;
      const res: any = await wait;
      const result: UajaxRes<T> = res[key];
      if (result) {
        const { ts, start_ts, traceid } = res;
        result.ts = ts;
        result.start_ts = start_ts;
        result.traceid = traceid;
      }
      return result;
    },
  };
}

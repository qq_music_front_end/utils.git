import { ClientRequest, IncomingMessage, OutgoingHttpHeader, request as httpRequest } from 'http';
import { RequestOptions, request as httpsRequest } from 'https';
import { isObject, isString } from '../type';

export interface SendRequestOptions extends RequestOptions {
  body?: any;
}

/**
 * 发送网络请求
 * @param url 要请求的链接
 * @param props
 * @returns res
 */
export async function sendRequest(url: string | URL, props: SendRequestOptions = {}): Promise<IncomingMessage> {
  return new Promise((resolve: (value: IncomingMessage) => void, reject: (reason?: any) => void): void => {
    if (!isString(url)) {
      url = url.toString();
    }
    const { body, ...options } = props;
    const { headers } = options;
    const request: ClientRequest = /^https/i.test(url)
      ? httpsRequest(url, options, resolve)
      : httpRequest(url, options, resolve);
    request.on('error', reject);
    if (body) {
      const contentType: OutgoingHttpHeader = headers?.['Content-Type'];
      if (contentType === 'application/json' && isObject(body)) {
        request.write(JSON.stringify(body));
      } else {
        request.write(body);
      }
    }
    request.end();
  });
}

export interface LoadResponseOptions {
  encoding?: BufferEncoding;
  json?: boolean;
}

export interface LoadResponseResult {
  status: number;
  response: any;
}

/**
 * 解析请求返回
 * @param res
 * @param opts
 * @returns 返回内容
 */
export async function loadResponse(res: IncomingMessage, opts: LoadResponseOptions): Promise<LoadResponseResult> {
  return new Promise((resolve: (value: LoadResponseResult) => void, reject: (reason?: any) => void): void => {
    const { encoding, json } = opts || {};
    res.setEncoding(isString(encoding) ? encoding : 'utf8');
    const { statusCode: status } = res;
    let response: string = '';
    res.on('data', (chunk: any): void => {
      response += chunk;
    });
    res.on('end', (): void => {
      try {
        resolve({
          status,
          response: json && status === 200 && response ? JSON.parse(response) : response,
        });
      } catch (error) {
        reject(error);
      }
    });
    res.on('error', reject);
  });
}

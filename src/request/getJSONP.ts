import { loadUrl } from './loadUrl';

export interface GetJsonpInit {
  /** 服务地址 */
  url: string;
  /** 请求数据 */
  data?: Record<string, any>;
  /** jsonp 回调函数名 */
  jsonpCallback?: string;
  /** jsonp 参数名，默认是 "callback" */
  jsonp?: string;
  /** 超时时间，默认不设置 */
  timeout?: number;
  /** 是否缓存，默认为 true */
  cache?: boolean;
}

/** JSONP回调自增id */
let jsonpID: number = 0;

/** 缓存 */
const map = {};

/**
 * 发送 JSONP 请求
 * @param init
 * @returns
 */
export async function getJSONP<T = any>(init: GetJsonpInit): Promise<T> {
  const { data, jsonpCallback, jsonp, timeout, cache } = init;
  let { url } = init;
  const jsonpUrl = new URL(url, location.href);
  const { searchParams } = jsonpUrl;
  const callbackName: string = jsonpCallback || `qmfe_jsonp_${++jsonpID}`;
  if (data) {
    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        searchParams.set(key, data[key]);
      }
    }
  }
  if (cache === false) {
    delete map[url];
  } else if (map[url]) {
    return map[url];
  }
  searchParams.set(jsonp || 'callback', callbackName);
  url = jsonpUrl.toString();
  map[url] = new Promise((resolve: (value: any) => void, reject: (reason?: any) => void): void => {
    let responseData: T;
    let timeoutTimer: number;
    window[callbackName] = (result: T): void => {
      responseData = result;
    };
    loadUrl(url, { type: 'script' }).then((success: boolean): void => {
      if (timeoutTimer > 0) {
        clearTimeout(timeoutTimer);
      }
      if (!success) {
        return reject('error');
      }
      resolve(responseData);
    });
    if (timeout && timeout > 0) {
      timeoutTimer = window.setTimeout(reject.bind(null, 'timeout'), timeout);
    }
  });
  return map[url];
}

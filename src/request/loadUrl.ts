import { createElement } from '../dom';
import { isString } from '../type';

export interface LoadUrlInit {
  /** 字符集设置，默认为 utf-8 */
  charset?: string;
  /** 是否冒泡，默认阻止 */
  bubble?: boolean;
  /** 资源类型，默认根据链接自动判断 */
  type?: 'script' | 'style';
}

/**
 * 加载 CSS 或 JS
 * @param url 链接
 * @param init 加载设置
 * @returns 是否加载成功
 */
export async function loadUrl(url: string, init?: string | LoadUrlInit): Promise<boolean> {
  return new Promise<boolean>((resolve: (value: boolean) => void): void => {
    if (isString(init)) {
      init = { charset: init };
    }
    const { charset, bubble, type } = init || {};
    const isCSS: boolean = type ? type === 'style' : /\.css/i.test(url.split(/\?|#/)[0]);
    const handle: EventListener = (event: Event): void => {
      if (!bubble) {
        event.stopPropagation();
      }
      const { target } = event;
      if (target) {
        target.removeEventListener('load', handle);
        target.removeEventListener('error', handle);
        if (!isCSS) {
          (target as Element).remove();
        }
      }
      resolve(event.type === 'load');
    };
    const props: Record<string, any> = {
      charset,
      onLoad: handle,
      onError: handle,
    };
    if (isCSS) {
      props.rel = 'stylesheet';
      props.href = url;
    } else {
      props.async = true;
      props.src = url;
    }
    document.body.append(createElement(isCSS ? 'link' : 'script', props));
  });
}

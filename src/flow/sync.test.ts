/**
 * @vitest-environment jsdom
 */

import { Mock, expect, test, vi } from 'vitest';
import { FlowCallBack } from './series';
import { sync } from './sync';

test('sync', async (): Promise<void> => {
  // 空任务
  const task1: number = await sync(1);
  expect(task1).toEqual(1);

  // 混合任务
  const task2: number = await sync<number>((next: FlowCallBack): void => {
    next(null, 1);
  });
  expect(task2).toEqual(1);

  // 错误
  const mockFn: Mock = vi.fn();
  try {
    await sync((next: FlowCallBack): void => {
      next('error');
    });
  } catch (error) {
    mockFn();
  }
  expect(mockFn).toHaveBeenCalledTimes(1);
});

/**
 * @vitest-environment jsdom
 */

import { Mock, describe, expect, test, vi } from 'vitest';
import { FlowCallBack, series } from './series';

describe.concurrent('series', (): void => {
  test('空任务', async (): Promise<void> => {
    const task1: any[] = await series([]);
    expect(task1).toEqual([]);
  });

  test('混合任务', async (): Promise<void> => {
    const mockFn1: Mock = vi.fn();
    const task2: any = await series([
      async (next: FlowCallBack): Promise<void> => {
        mockFn1();
        next(null, 1, 2);
      },
      async (next: FlowCallBack, ...value: any[]): Promise<void> => {
        expect(mockFn1).toHaveBeenCalledTimes(1);
        expect(value).toEqual([1, 2]);
        mockFn1();
        next();
      },
      2,
    ]);
    expect(mockFn1).toHaveBeenCalledTimes(2);
    expect(task2).toEqual([2]);
  });

  test('错误处理', async (): Promise<void> => {
    const mockFn2: Mock = vi.fn();
    try {
      await series([
        async (next: FlowCallBack): Promise<void> => {
          next('error');
        },
      ]);
    } catch (error) {
      mockFn2();
    }
    expect(mockFn2).toHaveBeenCalledTimes(1);
  });
});

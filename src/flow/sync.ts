import { isFunction } from '../type';
import { FlowTask } from './series';

/**
 * 以同步的方式执行
 * @param func
 * @returns
 */
export async function sync<T = any>(func: FlowTask): Promise<T> {
  if (!isFunction(func)) {
    return func as any;
  }
  return new Promise((resolve: (value: T) => void, reject: (reason?: any) => void): void => {
    func((error: any, data: T): void => {
      if (error) {
        return reject(error);
      }
      resolve(data);
    });
  });
}

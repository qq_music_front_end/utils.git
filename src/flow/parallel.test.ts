/**
 * @vitest-environment jsdom
 */

import { Mock, describe, expect, test, vi } from 'vitest';
import { parallel } from './parallel';
import { FlowCallBack } from './series';

describe.concurrent('parallel', (): void => {
  test('空任务', async (): Promise<void> => {
    const task1: any[] = await parallel([]);
    expect(task1).toEqual([]);
  });

  test('混合任务', async (): Promise<void> => {
    const task2: any[] = await parallel([
      async (next: FlowCallBack): Promise<void> => {
        next(null, 1);
      },
      2,
    ]);
    expect(task2).toEqual([1, 2]);
  });

  test('错误处理', async (): Promise<void> => {
    const mockFn: Mock = vi.fn();
    try {
      await parallel([
        async (next: FlowCallBack): Promise<void> => {
          next('error');
        },
      ]);
    } catch (error) {
      mockFn();
    }
    expect(mockFn).toHaveBeenCalledTimes(1);
  });
});

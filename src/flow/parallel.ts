import { isFunction } from '../type';
import { FlowTask } from './series';

/**
 * 并行流程控制，类似async.js
 * @param tasks 要执行的函数数组
 * @example
 *  await parallel([async (next) => next(null, 1), 2]);
 *  // => [1, 2]
 */
export async function parallel(tasks: FlowTask[]): Promise<any[]> {
  if (tasks.length == 0) {
    return [];
  }
  return await Promise.all(
    tasks.map((item: FlowTask): any => {
      if (!isFunction(item)) {
        return item;
      }
      return new Promise((resolve: (value: any) => void, reject: (reason?: any) => void): void => {
        item((error: any, result: any): void => {
          if (error) {
            return reject(error);
          }
          resolve(result);
        });
      });
    }),
  );
}

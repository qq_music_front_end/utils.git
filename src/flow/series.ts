import { isFunction } from '../type';

export type FlowCallBack = (error?: any, ...data: any) => void;
export type FlowTask = string | number | boolean | object | ((next: FlowCallBack) => unknown | Promise<unknown>);

/**
 * 串行流程控制，类似async.js
 * @param tasks 要执行的函数数组
 * @example
 *  await series([1, async (next, data) => next(null, data + 1)])
 *  // => 2
 */
export async function series(tasks: FlowTask[]): Promise<any> {
  if (tasks.length == 0) {
    return [];
  }
  let lastData: any[] = [];
  for (let index: number = 0; index < tasks.length; index++) {
    const item: FlowTask = tasks[index];
    if (!isFunction(item)) {
      lastData = [item];
      continue;
    }
    lastData = await new Promise<any[]>((resolve: (value: any[]) => void, reject: (reason?: any) => void): void => {
      item.apply(null, [
        (error: any, ...data: any[]): void => {
          if (error) {
            return reject(error);
          }
          resolve(data);
        },
        ...lastData,
      ]);
    });
  }
  return lastData;
}

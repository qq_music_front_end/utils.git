const map: Record<string, any> = {};

/**
 * 防抖
 * @param key
 * @param delay 延迟耗时，单位毫秒，默认为 0
 */
export async function debounce(key: string, delay: number = 0): Promise<void> {
  return new Promise((resolve: () => void): void => {
    if (map[key]) {
      clearTimeout(map[key]);
    }
    map[key] = setTimeout((): void => {
      delete map[key];
      resolve();
    }, delay);
  });
}

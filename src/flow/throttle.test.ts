/**
 * @vitest-environment jsdom
 */

import { Mock, expect, test, vi } from 'vitest';
import { sleep } from './sleep';
import { throttle } from './throttle';

test('throttle', async (): Promise<void> => {
  const mockFn: Mock = vi.fn();
  throttle('test1').then(mockFn);
  throttle('test1', 0).then(mockFn);
  expect(mockFn).toHaveBeenCalledTimes(0);
  await sleep(0);
  expect(mockFn).toHaveBeenCalledTimes(1);
}, 1000);

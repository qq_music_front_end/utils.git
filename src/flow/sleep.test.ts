import { expect, test } from 'vitest';
import { sleep } from './sleep';

test('sleep', async (): Promise<void> => {
  const startTime: number = Date.now();
  const duration = 10;
  await sleep(duration);
  expect(Date.now() - startTime).toBeGreaterThanOrEqual(duration);
});

test('sleep-no-param', async (): Promise<void> => {
  const startTime: number = Date.now();
  await sleep();
  expect(Date.now() - startTime).toBeGreaterThanOrEqual(0);
});

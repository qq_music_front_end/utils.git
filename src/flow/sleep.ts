/**
 * 等待指定时间后再继续执行
 * @param duration 等待时间，单位毫秒
 */
export async function sleep(duration = 0): Promise<void> {
  return new Promise((resolve: () => void): void => {
    setTimeout(resolve, duration);
  });
}

const map: Record<string, () => void> = {};

/**
 * 节流
 * @param key
 * @param delay 延迟耗时，单位毫秒，默认为 0
 */
export async function throttle(key: string, delay = 0): Promise<void> {
  return new Promise((resolve: () => void): void => {
    if (!map[key]) {
      setTimeout((): void => {
        const next: () => void = map[key];
        delete map[key];
        next();
      }, delay);
    }
    map[key] = resolve;
  });
}

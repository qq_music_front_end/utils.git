/**
 * @vitest-environment jsdom
 */

import { Mock, expect, test, vi } from 'vitest';
import { debounce } from './debounce';

test('debounce', async (): Promise<void> => {
  const mockFn1: Mock = vi.fn();
  const mockFn2: Mock = vi.fn();
  const mockFn3: Mock = vi.fn();
  debounce('test1').then(mockFn1);
  debounce('test1', 0).then(mockFn3);
  debounce('test1', 0).then(mockFn1);
  await Promise.all([
    // 之前的 test1 都不会执行
    debounce('test1', 0).then(mockFn1),
    debounce('test2', 0).then(mockFn2),
  ]);
  expect(mockFn1).toHaveBeenCalledTimes(1);
  expect(mockFn2).toHaveBeenCalledTimes(1);
  expect(mockFn3).toHaveBeenCalledTimes(0);
});

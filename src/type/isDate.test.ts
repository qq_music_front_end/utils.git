import { expectRun } from '../test';
import { isDate } from './isDate';

const date = new Date();

expectRun({
  name: 'isDate',
  handle: isDate,
  cases: [
    {
      name: 'Date',
      source: new Date(),
      target: true,
    },
    {
      name: 'string',
      source: date.toString(),
      target: false,
    },
    {
      name: 'timestamp',
      source: date.valueOf(),
      target: false,
    },
  ],
});

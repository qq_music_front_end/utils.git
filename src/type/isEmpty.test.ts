import { expectRun } from '../test';
import { isEmpty } from './isEmpty';

const date = new Date();

expectRun({
  name: 'isEmpty',
  handle: isEmpty,
  cases: [
    {
      name: 'null',
      source: null,
      target: true,
    },
    {
      name: 'false',
      source: false,
      target: true,
    },
    {
      name: '{}',
      source: {},
      target: true,
    },
    {
      name: '{ key: 1 }',
      source: { key: 1 },
      target: false,
    },
    {
      name: '1',
      source: 1,
      target: false,
    },
    {
      name: '[1, 2, 3]',
      source: [1, 2, 3],
      target: false,
    },
    {
      name: 'string',
      source: 'abc',
      target: false,
    },
    {
      name: '正则',
      source: /./,
      target: false,
    },
  ],
});

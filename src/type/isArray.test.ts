import { expectRun } from '../test';
import { isArray } from './isArray';

expectRun({
  name: 'isArray',
  handle: isArray,
  cases: [
    {
      name: 'array',
      source: [1, 2, 3],
      target: true,
    },
    {
      name: 'Uint8Array',
      source: new Uint8Array([1, 2, 3]),
      target: false,
    },
  ],
});

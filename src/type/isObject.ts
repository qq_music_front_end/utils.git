import { checkType } from './checkType';

/**
 * 判断 `value` 是否是 `Object` 类型.
 * @param value 需要判断的值
 * @returns 如果是则返回true，否则返回false
 * @example
 *
 * isObject({})
 * // => true
 *
 * isObject(1)
 * // => false
 *
 * isObject([1, 2, 3])
 * // => false
 *
 * isObject(Function)
 * // => false
 *
 * isObject(null)
 * // => false
 */
export function isObject<T extends object>(value: any): value is T {
  return checkType(value, 'Object');
}

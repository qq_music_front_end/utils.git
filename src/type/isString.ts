import { checkType } from './checkType';

/**
 * 判断 `value` 是 `String` primitive 或者 object
 *
 * @param value 需要判断的值
 * @returns 如果是则返回true，否则返回false
 * @example
 *
 * isString('abc')
 * // => true
 *
 * isString(new String())
 * // => true
 *
 * isString(1)
 * // => false
 */
export function isString(value: any): value is string {
  return checkType(value, 'String');
}

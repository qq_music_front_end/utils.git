import { expectRun } from '../test';
import { isNumber } from './isNumber';

expectRun({
  name: 'isNumber',
  handle: isNumber,
  cases: [
    {
      name: '1',
      source: 1,
      target: true,
    },
    {
      name: '"1"',
      source: '1',
      target: false,
    },
    {
      name: 'Infinity',
      source: Infinity,
      target: true,
    },
    {
      name: 'NaN',
      source: NaN,
      target: true,
    },
    {
      name: 'bigint',
      source: 123n,
      target: true,
    },
  ],
});

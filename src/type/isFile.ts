import { checkType } from './checkType';

/**
 * 判断 `value` 是否是 `文件对象`.
 *
 * @param value 要检查的值
 * @returns 如果是则返回true，否则返回false
 */
export function isFile(value: any): value is File {
  return checkType(value, 'File');
}

import { checkType } from './checkType';

/**
 * 判断 `value` 是否是 `Date` 类型
 *
 * @param value 需要判断的值
 * @returns 如果是则返回true，否则返回false
 * @example
 *
 * isDate(new Date);
 * // => true
 *
 * isDate('Mon Aprial 23 2012');
 * // => false
 *
 */
export function isDate(value: any): value is Date {
  return checkType(value, 'Date');
}

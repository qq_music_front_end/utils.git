/**
 * 判断 `value` 是否是 `Array` 类型
 *
 * @param value 需要判断的值
 * @returns 如果是则返回true，否则返回false
 * @example
 *
 * isArray([1, 2, 3]);
 * // => true
 *
 * isArray(document.children);
 * // => false
 *
 * isArray('abc');
 * // => false
 */
export function isArray<T = any>(value: any): value is T[] {
  return Array.isArray(value);
}

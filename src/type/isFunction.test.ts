import { expectRun } from '../test';
import { isFunction } from './isFunction';

expectRun(
  {
    name: 'isFunction',
    handle: isFunction,
    cases: [
      {
        name: 'function',
        source: (): void => {},
        target: true,
      },
      {
        name: 'async function',
        source: async (): Promise<void> => {},
        target: true,
      },
    ],
  },
);

import { expectRun } from '../test';
import { isUndefined } from './isUndefined';

expectRun({
  name: 'isUndefined',
  handle: isUndefined,
  cases: [
    {
      name: 'undefined',
      source: void 0,
      target: true,
    },
    {
      name: 'null',
      source: null,
      target: false,
    },
  ],
});

import { expectRun } from '../test';
import { isRegExp } from './isRegExp';

expectRun({
  name: 'isRegExp',
  handle: isRegExp,
  cases: [
    {
      name: 'new RegExp',
      source: new RegExp('.'),
      target: true,
    },
    {
      name: '/./',
      source: /./,
      target: true,
    },
  ],
});

import { checkType } from './checkType';

/**
 * 判断 `value` 是否是 `Boolean` 类型
 *
 * @param value 需要判断的值
 * @returns 如果是则返回true，否则返回false
 * @example
 *
 * isBoolean(false)
 * // => true
 *
 * isBoolean(null)
 * // => false
 */

export function isBoolean(value: any): value is boolean {
  return value === true || value === false || checkType(value, 'Boolean');
}

import { checkType } from './checkType';

/**
 * 判断 `value` 是否是 `Promise`.
 *
 * @param value 要检查的值
 * @returns 如果是则返回true，否则返回false
 */
export function isPromise<T = any>(value: any): value is Promise<T> {
  return checkType(value, 'Promise');
}

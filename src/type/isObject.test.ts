import { expectRun } from '../test';
import { isObject } from './isObject';

expectRun({
  name: 'isObject',
  handle: isObject,
  cases: [
    {
      name: '{}',
      source: {},
      target: true,
    },
    {
      name: '[1, 2, 3]',
      source: [1, 2, 3],
      target: false,
    },
    {
      name: '1',
      source: 1,
      target: false,
    },
  ],
});

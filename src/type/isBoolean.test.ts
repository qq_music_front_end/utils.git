import { expectRun } from '../test';
import { isBoolean } from './isBoolean';

expectRun({
  name: 'isBoolean',
  handle: isBoolean,
  cases: [
    {
      name: 'true',
      source: true,
      target: true,
    },
    {
      name: '1',
      source: 1,
      target: false,
    },
    {
      name: 'null',
      source: null,
      target: false,
    },
  ],
});

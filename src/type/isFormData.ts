import { checkType } from './checkType';

/**
 * 判断 `value` 是否是 `FormData`.
 *
 * @param value 要检查的值
 * @returns 如果是则返回true，否则返回false
 */
export function isFormData(value: any): value is FormData {
  return checkType(value, 'FormData');
}

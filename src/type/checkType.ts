/**
 * 通过 `Object.prototype.toString` 方法检测对象类型
 *
 * @param value 要检查的值
 * @param type 类型
 * @returns 如果是则返回true，否则返回false
 */
export function checkType(value: any, type: string): boolean {
  return Object.prototype.toString.call(value) === `[object ${type}]`;
}

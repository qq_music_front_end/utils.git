/**
 * 判断 `value` 是否是 `Number` 类型
 *
 * **Note:** To exclude `Infinity`, `-Infinity`, and `NaN`, which are
 * classified as numbers, use the `isFinite` method.
 *
 * @param value 需要判断的值
 * @returns 如果是则返回true，否则返回false
 * @example
 *
 * isNumber(3)
 * // => true
 *
 * isNumber(Number.MIN_VALUE)
 * // => true
 *
 * isNumber(Infinity)
 * // => true
 *
 * isNumber('3')
 * // => false
 */
export function isNumber(value: any): value is number {
  const type: string = typeof value;
  return type === 'number' || type === 'bigint';
}

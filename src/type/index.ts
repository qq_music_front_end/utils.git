/**
 * @file 类型判断
 */

export * from './checkType';
export * from './isArray';
export * from './isBoolean';
export * from './isDate';
export * from './isEmpty';
export * from './isFile';
export * from './isFormData';
export * from './isFunction';
export * from './isNull';
export * from './isNumber';
export * from './isObject';
export * from './isPromise';
export * from './isRegExp';
export * from './isString';
export * from './isTrueEmpty';
export * from './isUndefined';

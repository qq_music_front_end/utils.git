import { expectRun } from '../test';
import { isTrueEmpty } from './isTrueEmpty';

expectRun({
  name: 'isTrueEmpty',
  handle: isTrueEmpty,
  cases: [
    {
      name: 'null',
      source: null,
      target: true,
    },
    {
      name: 'undefined',
      source: void 0,
      target: true,
    },
    {
      name: 'NaN',
      source: NaN,
      target: true,
    },
    {
      name: '""',
      source: '',
      target: true,
    },
    {
      name: 'false',
      source: false,
      target: false,
    },
    {
      name: '{}',
      source: {},
      target: false,
    },
    {
      name: '0',
      source: 0,
      target: false,
    },
  ],
});

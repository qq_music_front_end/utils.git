/**
 * 判断 `value` 是否是 `null` 值.
 *
 * @param value 要检查的值
 * @returns 如果是则返回true，否则返回false
 * @example
 *
 * isNull(null)
 * // => true
 *
 * isNull(void 0)
 * // => false
 *
 * isNull(NaN)
 * // => false
 */
export function isNull(value: any): value is null {
  return value === null;
}

import { isNumber } from './isNumber';

/**
 *
 * 判断 `value` 是否是 `null` `undefined` `''` `NaN`
 * @param value 要判断的值
 * @returns 如果是则返回true，否则返回false
 * @example
 *
 * isTrueEmpty(void 0)
 * // => true
 *
 * isTrueEmpty(null)
 * // => true
 *
 * isTrueEmpty('')
 * // => true
 *
 * isTrueEmpty(NaN)
 * // => true
 *
 * isTrueEmpty(0)
 * // => false
 */
export function isTrueEmpty(value: any): boolean {
  return value === null || value === undefined || value === '' || (isNumber(value) && isNaN(value));
}

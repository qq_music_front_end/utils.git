import { expectRun } from '../test';
import { isNull } from './isNull';

expectRun({
  name: 'isNull',
  handle: isNull,
  cases: [
    {
      name: 'null',
      source: null,
      target: true,
    },
    {
      name: 'void 0',
      source: void 0,
      target: false,
    },
    {
      name: 'NaN',
      source: NaN,
      target: false,
    },
  ],
});

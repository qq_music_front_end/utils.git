/**
 * 判断 `value` 是否是 `Function` 类型
 *
 * @param value 需要判断的值
 * @returns 如果是则返回true，否则返回false
 * @example
 *
 * isFunction(async function() {})
 * // => true
 *
 * isFunction(function() {})
 * // => true
 *
 * isFunction(/abc/)
 * // => false
 *
 * isFunction({})
 * // => false
 */

export function isFunction<T = Function>(value: any): value is T {
  return typeof value === 'function';
}

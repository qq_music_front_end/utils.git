import { expectRun } from '../test';
import { isFormData } from './isFormData';

expectRun({
  name: 'isFormData',
  handle: isFormData,
  cases: [
    {
      name: '{}',
      source: {},
      target: false,
    },
  ],
});

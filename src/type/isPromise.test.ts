import { expectRun } from '../test';
import { isPromise } from './isPromise';

expectRun({
  name: 'isPromise',
  handle: isPromise,
  cases: [
    {
      name: 'new Promise',
      source: new Promise((): void => {}),
      target: true,
    },
    {
      name: 'Promise.resolve()',
      source: Promise.resolve(),
      target: true,
    },
    {
      name: 'async function',
      source: (async (): Promise<void> => {})(),
      target: true,
    },
  ],
});

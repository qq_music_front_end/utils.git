import { expectRun } from '../test';
import { isString } from './isString';

expectRun({
  name: 'isString',
  handle: isString,
  cases: [
    {
      name: '"abc"',
      source: 'abc',
      target: true,
    },
    {
      name: 'new String',
      source: new String(),
      target: true,
    },
  ],
});

import { isArray } from './isArray';
import { isBoolean } from './isBoolean';
import { isNumber } from './isNumber';
import { isObject } from './isObject';
import { isString } from './isString';
import { isTrueEmpty } from './isTrueEmpty';

/**
 *
 * 判断 `value` 是否是一个空的object, collection, map, 或者 set.
 *
 * 没有可枚举属性的 `object` 则被称为空
 * Array-like类型的如 `arguments`, `objects`, ` arrays`, `strings`, 如果其 `length` 等于 `0`, 则被称为空
 * Map 和 Set 的 `size` 等于 `0` 则被称为空
 *
 * 这里和lodash的实现有些差别，去掉了对 `Buffer`、`TypedArray`等不常用类型的判断
 *
 * @param value 要判断的值，
 * @returns 如果是则返回true，否则返回false
 * @example
 *
 * isEmpty(null)
 * // => true
 *
 * isEmpty({})
 * // => true
 *
 * isEmpty(true)
 * // => true
 *
 * isEmpty(1)
 * // => false
 *
 * isEmpty([1, 2, 3])
 * // => false
 *
 * isEmpty('abc')
 * // => false
 *
 * isEmpty({ 'a': 1 })
 * // => false
 */
export function isEmpty(value: any): boolean {
  if (isTrueEmpty(value)) {
    return true;
  }
  if (isNumber(value)) {
    return value === 0;
  }
  if (isBoolean(value)) {
    return !value;
  }
  if (isString(value) || isArray(value)) {
    return value.length === 0;
  }
  if (isObject(value)) {
    for (const key in value) {
      return false;
    }
    return true;
  }
  return false;
}

import { expectRun } from '../test';
import { isFile } from './isFile';

expectRun({
  name: 'isFile',
  handle: isFile,
  cases: [
    {
      name: '{}',
      source: {},
      target: false,
    },
  ],
});

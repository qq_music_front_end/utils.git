/**
 * 判断 `value` 是 `undefined`.
 *
 * @param value 要检查的值
 * @returns 如果是则返回true，否则返回false
 * @example
 *
 * isUndefined(null)
 * // => false
 *
 * isUndefined(void 0)
 * // => true
 *
 * isUndefined(undefined)
 * // => true
 */
export function isUndefined(value: any): value is undefined {
  return value === undefined;
}

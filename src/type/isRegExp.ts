import { checkType } from './checkType';

/**
 * 判断 `value` 是否是 `正则表达式`.
 *
 * @param value 要检查的值
 * @returns 如果是则返回true，否则返回false
 */
export function isRegExp(value: any): value is RegExp {
  return checkType(value, 'RegExp');
}

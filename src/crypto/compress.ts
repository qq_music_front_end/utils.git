const BASE62_TABLE: string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
const TABLE_LENGTH = 62; // BASE62_TABLE.length;
const BUFFER_MAX = 61; // TABLE_LENGTH - 1;

/** 滑动窗口大小 */
const WINDOW_MAX = 1024;
const WINDOW_BUFFER_MAX = 304; // maximum 304

/** Function.prototype.apply 堆栈最大值 */
const APPLY_BUFFER_SIZE = 65533;

/** chunk 大小 */
const COMPRESS_CHUNK_MAX = 65471; // APPLY_BUFFER_SIZE - TABLE_LENGTH;

/** latin 字符个数 : U+0000 - U+0084 */
const LATIN_BUFFER_MAX = 0x84;

// Unicode table : U+0000 - U+FFFF
const UNICODE_CHAR_MAX = 40;
const UNICODE_BUFFER_MAX = 1640; // UNICODE_CHAR_MAX * (UNICODE_CHAR_MAX + 1);

// Index positions
const LATIN_INDEX = 63; // TABLE_LENGTH + 1;
const LATIN_INDEX_START = 20;
const UNICODE_INDEX = 67; // TABLE_LENGTH + 5;

// Decode/Start positions
const DECODE_MAX = 43; // TABLE_LENGTH - 19;
const LATIN_DECODE_MAX = 47; // UNICODE_CHAR_MAX + 7;
const CHAR_START = 48; // LATIN_DECODE_MAX + 1;
const COMPRESS_START = 49; // CHAR_START + 1;
const COMPRESS_FIXED_START = 54; // COMPRESS_START + 5;
const COMPRESS_INDEX = 59; // COMPRESS_FIXED_START + 5; // 59
// Currently, 60 and 61 of the position is not used yet

function sliceBuffer(buffer: Uint8Array | number[], begin: number, end: number): Uint8Array | number[] {
  if (begin === 0 && buffer.length === end) {
    return buffer;
  }

  if ((buffer as Uint8Array).subarray) {
    return (buffer as Uint8Array).subarray(begin, end);
  }

  return (buffer as number[]).slice(begin, end);
}

/** 将 Uint8Array 转为字符串 */
function bufferToString(buffer: Uint8Array | number[], length: number): string {
  const { fromCharCode } = String;
  buffer = sliceBuffer(buffer, 0, length);

  // 先尝试使用系统自带方法转换，失败的话再手动转换
  try {
    return fromCharCode.apply(null, buffer);
  } catch (ex) {
    // 直接转换失败，尝试分割成片段进行转换
  }

  let string = '';
  let index: number = 0;
  let sub: Uint8Array | number[];

  while (index < length) {
    sub = sliceBuffer(buffer, index, index + APPLY_BUFFER_SIZE);
    index += APPLY_BUFFER_SIZE;

    try {
      string += fromCharCode.apply(null, sub);
    } catch (ex) {
      for (let i: number = 0; i < length; i++) {
        string += fromCharCode(buffer[i]);
      }
    }
  }

  return string;
}

class Compressor {
  createTable(): Uint8Array {
    const table: Uint8Array = new Uint8Array(TABLE_LENGTH);
    for (let i: number = 0; i < TABLE_LENGTH; i++) {
      table[i] = BASE62_TABLE.charCodeAt(i);
    }
    return table;
  }

  // Search for a longest match
  search(data: string, offset: number): { index: number; length?: number } {
    const length: number = data.length;
    let i: number = 2;
    let len: number = BUFFER_MAX;
    if (length - offset < len) {
      len = length - offset;
    }
    if (i > len) {
      return { index: -1 };
    }

    let pos: number = offset - WINDOW_BUFFER_MAX;
    let win: string = data.substring(pos, offset + len);
    let limit: number = offset + i - 3 - pos;
    let j: number;
    let s: string;
    let index: number;
    let bestIndex: number;

    do {
      if (i === 2) {
        s = data.charAt(offset) + data.charAt(offset + 1);

        // Fast check by pre-match for the slow lastIndexOf.
        index = win.indexOf(s);
        if (index === -1 || index > limit) {
          break;
        }
      } else if (i === 3) {
        s = s + data.charAt(offset + 2);
      } else {
        s = data.substring(offset, i);
      }

      const lastIndex: number = win.lastIndexOf(s, limit);
      if (lastIndex === -1) {
        break;
      }

      bestIndex = lastIndex;
      j = pos + lastIndex;
      do {
        if (data.charCodeAt(offset + i) !== data.charCodeAt(j + i)) {
          break;
        }
      } while (++i < len);

      if (index === lastIndex) {
        i++;
        break;
      }
    } while (++i < len);

    if (i === 2) {
      return { index: -1 };
    }

    return {
      index: WINDOW_BUFFER_MAX - bestIndex,
      length: i - 1,
    };
  }

  compress(data: string): string {
    if (!data?.length) {
      return '';
    }

    let result: string = '';
    const table: Uint8Array = this.createTable();
    let win: string = ''.padStart(WINDOW_MAX, ' ');
    const buffer: Uint8Array = new Uint8Array(APPLY_BUFFER_SIZE);
    let count: number = 0;

    data = win + data;
    let offset: number = win.length;
    const dataLen: number = data.length;

    let index: number = -1;
    let lastIndex: number = -1;
    let c1: number;
    let c2: number;
    let c3: number;
    let c4: number;

    while (offset < dataLen) {
      const { index: searchIndex, length: searchLength } = this.search(data, offset);
      if (searchIndex === -1) {
        const c: number = data.charCodeAt(offset++);
        if (c < LATIN_BUFFER_MAX) {
          if (c < UNICODE_CHAR_MAX) {
            c1 = c;
            c2 = 0;
            index = LATIN_INDEX;
          } else {
            c1 = c % UNICODE_CHAR_MAX;
            c2 = (c - c1) / UNICODE_CHAR_MAX;
            index = c2 + LATIN_INDEX;
          }

          // Latin index
          if (lastIndex === index) {
            buffer[count++] = table[c1];
          } else {
            buffer[count++] = table[index - LATIN_INDEX_START];
            buffer[count++] = table[c1];
            lastIndex = index;
          }
        } else {
          if (c < UNICODE_BUFFER_MAX) {
            c1 = c;
            c2 = 0;
            index = UNICODE_INDEX;
          } else {
            c1 = c % UNICODE_BUFFER_MAX;
            c2 = (c - c1) / UNICODE_BUFFER_MAX;
            index = c2 + UNICODE_INDEX;
          }

          if (c1 < UNICODE_CHAR_MAX) {
            c3 = c1;
            c4 = 0;
          } else {
            c3 = c1 % UNICODE_CHAR_MAX;
            c4 = (c1 - c3) / UNICODE_CHAR_MAX;
          }

          // Unicode index
          if (lastIndex === index) {
            buffer[count++] = table[c3];
            buffer[count++] = table[c4];
          } else {
            buffer[count++] = table[CHAR_START];
            buffer[count++] = table[index - TABLE_LENGTH];
            buffer[count++] = table[c3];
            buffer[count++] = table[c4];

            lastIndex = index;
          }
        }
      } else {
        if (searchIndex < BUFFER_MAX) {
          c1 = searchIndex;
          c2 = 0;
        } else {
          c1 = searchIndex % BUFFER_MAX;
          c2 = (searchIndex - c1) / BUFFER_MAX;
        }

        if (searchLength === 2) {
          buffer[count++] = table[c2 + COMPRESS_FIXED_START];
          buffer[count++] = table[c1];
        } else {
          buffer[count++] = table[c2 + COMPRESS_START];
          buffer[count++] = table[c1];
          buffer[count++] = table[searchLength];
        }

        offset += searchLength;
        if (~lastIndex) {
          lastIndex = -1;
        }
      }

      if (count >= COMPRESS_CHUNK_MAX) {
        result += bufferToString(buffer, count);
        count = 0;
      }
    }

    if (count > 0) {
      result += bufferToString(buffer, count);
    }

    return result === null ? '' : result;
  }
}

class Decompressor {
  createTable(): Record<string, number> {
    const table: Record<string, number> = {};
    for (let i: number = 0; i < TABLE_LENGTH; i++) {
      table[BASE62_TABLE.charAt(i)] = i;
    }
    return table;
  }

  decompress(data: string): string {
    if (data == null || data.length === 0) {
      return '';
    }

    let result: number[] = [];
    for (let index: number = 0; index < WINDOW_MAX; index++) {
      result[index] = 32;
    }
    const table: Record<string, number> = this.createTable();
    let out: boolean = false;
    let index: number = null;
    let len: number = data.length;
    let offset: number = 0;
    let c: number;
    let c2: number;
    let c3: number;
    let code: number;

    for (; offset < len; offset++) {
      c = table[data.charAt(offset)];
      if (c === void 0) {
        continue;
      }

      if (c < DECODE_MAX) {
        if (!out) {
          // Latin index
          code = index * UNICODE_CHAR_MAX + c;
        } else {
          // Unicode index
          c3 = table[data.charAt(++offset)];
          code = c3 * UNICODE_CHAR_MAX + c + UNICODE_BUFFER_MAX * index;
        }
        result[result.length] = code;
      } else if (c < LATIN_DECODE_MAX) {
        // Latin starting point
        index = c - DECODE_MAX;
        out = false;
      } else if (c === CHAR_START) {
        // Unicode starting point
        c2 = table[data.charAt(++offset)];
        index = c2 - 5;
        out = true;
      } else if (c < COMPRESS_INDEX) {
        c2 = table[data.charAt(++offset)];
        const start: number = c < COMPRESS_FIXED_START ? COMPRESS_START : COMPRESS_FIXED_START;
        const pos: number = (c - start) * BUFFER_MAX + c2;
        const length: number = c < COMPRESS_FIXED_START ? table[data.charAt(++offset)] : 2;
        const sub: number[] = result.slice(-pos);
        if (sub.length > length) {
          sub.length = length;
        }
        const subLen: number = sub.length;

        if (subLen > 0) {
          let expandLen: number = 0;
          while (expandLen < length) {
            for (let i: number = 0; i < subLen; i++) {
              result[result.length] = sub[i];
              if (++expandLen >= length) {
                break;
              }
            }
          }
        }
        index = null;
      }
    }

    result = result.slice(WINDOW_MAX);
    return bufferToString(result, result.length);
  }
}

/**
 * LZSS 字符串压缩
 * @param data 要压缩的字符串
 * @returns Base62 编码（0-9a-zA-Z）
 */
export function compress(data: string): string {
  return new Compressor().compress(data);
}

/**
 * LZSS 字符串解压
 * @param data Base62 编码（0-9a-zA-Z） 的字符串
 * @returns 解压后的字符串
 */
export function decompress(data: string): string {
  return new Decompressor().decompress(data);
}

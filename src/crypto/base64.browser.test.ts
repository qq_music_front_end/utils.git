/**
 * @vitest-environment jsdom
 */

import { TextDecoder, TextEncoder } from 'util';
import { TestItem, expectRun } from '../test';
import { decodeBase64, encodeBase64 } from './base64';

globalThis.TextEncoder = TextEncoder;
globalThis.TextDecoder = TextDecoder as any;

const cases: TestItem[] = [
  {
    name: '中文',
    source:
      '北国风光，千里冰封，万里雪飘。望长城内外，惟余莽莽；大河上下，顿失滔滔。山舞银蛇，原驰蜡象，欲与天公试比高。须晴日，看红装素裹，分外妖娆。江山如此多娇，引无数英雄竞折腰。惜秦皇汉武，略输文采；唐宗宋祖，稍逊风骚。一代天骄，成吉思汗，只识弯弓射大雕。俱往矣，数风流人物，还看今朝。',
    target:
      '5YyX5Zu96aOO5YWJ77yM5Y2D6YeM5Yaw5bCB77yM5LiH6YeM6Zuq6aOY44CC5pyb6ZW/5Z+O5YaF5aSW77yM5oOf5L2Z6I696I6977yb5aSn5rKz5LiK5LiL77yM6aG/5aSx5ruU5ruU44CC5bGx6Iie6ZO26JuH77yM5Y6f6amw6Jyh6LGh77yM5qyy5LiO5aSp5YWs6K+V5q+U6auY44CC6aG75pm05pel77yM55yL57qi6KOF57Sg6KO577yM5YiG5aSW5aaW5aiG44CC5rGf5bGx5aaC5q2k5aSa5aiH77yM5byV5peg5pWw6Iux6ZuE56ue5oqY6IWw44CC5oOc56em55qH5rGJ5q2m77yM55Wl6L6T5paH6YeH77yb5ZSQ5a6X5a6L56WW77yM56iN6YCK6aOO6aqa44CC5LiA5Luj5aSp6aqE77yM5oiQ5ZCJ5oCd5rGX77yM5Y+q6K+G5byv5byT5bCE5aSn6ZuV44CC5L+x5b6A55+j77yM5pWw6aOO5rWB5Lq654mp77yM6L+Y55yL5LuK5pyd44CC',
  },
  {
    name: '混合',
    source: 'a Ā 𐀀 文 🦄',
    target: 'YSDEgCDwkICAIOaWhyDwn6aE',
  },
  {
    name: '空字符串',
    source: '',
    target: '',
  },
];

expectRun({
  name: 'Base64.encode',
  handle: encodeBase64,
  cases,
});

expectRun({
  name: 'Base64.decode',
  handle: decodeBase64,
  cases: cases.map(({ name, source, target }: TestItem): TestItem => {
    return {
      name,
      source: target,
      target: source,
    };
  }),
});

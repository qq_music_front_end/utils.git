import { expect } from 'vitest';
import { expectRun } from '../test';
import { createHash } from './createHash';

const source: string =
  '北国风光，千里冰封，万里雪飘。望长城内外，惟余莽莽；大河上下，顿失滔滔。山舞银蛇，原驰蜡象，欲与天公试比高。须晴日，看红装素裹，分外妖娆。江山如此多娇，引无数英雄竞折腰。惜秦皇汉武，略输文采；唐宗宋祖，稍逊风骚。一代天骄，成吉思汗，只识弯弓射大雕。俱往矣，数风流人物，还看今朝。';

expectRun({
  name: 'createHash',
  handle: createHash,
  cases: [
    {
      name: 'sha256',
      source,
      target: 'c4773e4bfcce5cc28ea843bb0538cf8bd106d672a4ae5e3cb0a2769fcca17a2f',
    },
    {
      name: 'md5',
      option: 'md5',
      source,
      target: '4f7d9ab867ac776fbbf9c7346a7caf5f',
    },
    {
      name: '无效数据',
      source: 1234,
      target: '',
    },
    {
      name: '异常抛出',
      handle(source: string): any {
        expect((): any => createHash(source, 'md5', { error: true })).toThrowError('argument must be of type string');
      },
      source: 1234,
      target: undefined,
    },
  ],
});

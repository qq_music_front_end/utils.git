import { TestItem, expectRun } from '../test';
import { compress, decompress } from './compress';

const cases: TestItem[] = [
  {
    name: '中文',
    source:
      '北国风光，千里冰封，万里雪飘。望长城内外，惟余莽莽；大河上下，顿失滔滔。山舞银蛇，原驰蜡象，欲与天公试比高。须晴日，看红装素裹，分外妖娆。江山如此多娇，引无数英雄竞折腰。惜秦皇汉武，略输文采；唐宗宋祖，稍逊风骚。一代天骄，成吉思汗，只识弯弓射大雕。俱往矣，数风流人物，还看今朝。',
    target:
      'wRfnwSdXwcmiwRJcwsMhwRjowbEfwRgewThOwsMhwRPHwbEfwciWIjwMKUwVjDwcfNwSmcwRddwSGlwsMhwUnEwRhPwZFXFXwsbhwSXlwVbnwRSHTHwsMhwcngwShlwWMMMMwMKUwTZRwZeMwcOKwaXBwsMhwSHCwdQEwahDRlwsMhwVCewRWHwSZlwREdwalhwVEiwdAHwMKUwcjgwUcoFlwsMhwXjXwYSfwaNOwYgVwaZPwsMhwRmgwSGlOowTGCwMKUwVXlwTZRwSinwVMfwSKlwTHCwsMhwTViwUAlIiwZBSwckVwYOHwUAQwZQIwMKUwUkEwYGBwXXUwVBlOfwsMhwXNNwbjQwUfiwbnewsbhwSQLwTHMjLwXWmwsMhwYFCwbiTwcmiwdSFwMKUwRIHjMwSZlwdkEwsMhwUYMwShEwUFAwVPlwsMhwSCEwaWhwTHjTikOwSXlwcNWwMKUwRZTwTIlwXLdwsMhwUIiwcmiwWJCwRiLwWJjwsMhwbYSwXjXwRKMwVlDwMKU',
  },
  {
    name: '英文',
    source: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/112.0',
    target:
      'sltfuCtZccRsHNGIrgsAtHZeUfnjrgsmtErgsJI2RsTrgxRDsOM2HuAxFEtimsS2VsR2WsBrgsftVTbfsHKI2PxDDsJrgsetZiVWfuAsHJJK2e',
  },
  {
    name: '空字符串',
    source: '',
    target: '',
  },
];

expectRun({
  name: 'Compressor',
  handle: compress,
  cases,
});

expectRun({
  name: 'Decompressor',
  handle: decompress,
  cases: cases.map(({ name, source, target }: TestItem): TestItem => {
    return {
      name,
      source: target,
      target: source,
    };
  }),
});

import { BinaryLike, createHash as cryptoCreateHash, Hash } from 'crypto';

export interface CreateHashOptions {
  error?: boolean;
}

/**
 * 计算哈希值，默认使用 Sha256 算法
 * @param str 待计算的数据
 * @param algorithm 算法类型
 * @return 16 进制哈希值
 */
export function createHash(str: BinaryLike, algorithm: string = 'sha256', options?: CreateHashOptions): string {
  try {
    const hash: Hash = cryptoCreateHash(algorithm);
    hash.update(str);
    return hash.digest('hex');
  } catch (ex) {
    if (options?.error) {
      throw ex;
    }
    return '';
  }
}

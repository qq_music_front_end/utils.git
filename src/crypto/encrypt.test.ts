import { TestItem, expectRun } from '../test';
import { decrypt, encrypt } from './encrypt';

const cases: TestItem[] = [
  {
    name: 'aes-256-cbc',
    option: 'aes-256-cbc',
    source:
      '北国风光，千里冰封，万里雪飘。望长城内外，惟余莽莽；大河上下，顿失滔滔。山舞银蛇，原驰蜡象，欲与天公试比高。须晴日，看红装素裹，分外妖娆。江山如此多娇，引无数英雄竞折腰。惜秦皇汉武，略输文采；唐宗宋祖，稍逊风骚。一代天骄，成吉思汗，只识弯弓射大雕。俱往矣，数风流人物，还看今朝。',
    target:
      '21bb7d539f7fec6a47b1763c7b5b252e76319f9a93d3fd01ffdf0b62b0771b2e7c56aebd5e381882b65d39d9f95274e4443e0d80afb46b7d7431423aebe8cff27feb884e497b3bf8f710363eeaa09a06eeb142cf5697337b0b22e7d851e095b636f0b2d3b272ef3c9f04b87d49442c13b8621e8d2c8500904025eb94fa6fb8509279a8c0a0ef9beb1026985ff8c700c39fb3fbf997b1834d4e8166d9d2f45aee5d0fcc515d6944cba535ba566ea1d31107cf3fa83c8bb821e44df8486eddcba5ec3b8942e74225a9dea6de85c0918935d3415c1dde04cefc671b721a02bfeee2fbf076380f3855bd0bdde929a1189a9ad646d7767ef908a4236af754f3fdd1386165b20e3bda13ae1022ea0cee69baf11f869a7ecac3d7bfad810e6fa08dfedce4984a5fdeb0bfd8240de2969ca75603158ba9819d242a5c1e2bb88857ad9c39066d4a6a0f0fa1a77b90d649ce67d8fd6fb8a2f7f5e811979e977578b11b6cad546f9997e4a53f49d929b9074d8f1db18fd15040c3bb4959895c6289e37ac9938e39ce3477c521edd726cc59d197790da6a3f0dc9ad18611ed792d47269744d10dba4b1c64ba30c956b2cd4662d2b43e',
  },
];

expectRun({
  name: 'encrypt',
  handle: encrypt,
  cases,
});

expectRun({
  name: 'decrypt',
  handle: decrypt,
  cases: cases.map(({ name, source, target }: TestItem): TestItem => {
    return {
      name,
      source: target,
      target: source,
    };
  }),
});

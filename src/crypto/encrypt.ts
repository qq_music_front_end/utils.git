import { BinaryLike, createCipheriv, createDecipheriv, Encoding } from 'crypto';

export const ALG = 'aes-256-cbc';
export const KEY = 'bf9917c19e0cb07477d9bc27b13c1470';
export const IV: Buffer = Buffer.from(KEY, 'hex');

/**
 * 字符串加密
 * @param str 待加密的字符串
 * @param algorithm 加密算法，默认 aes-256-cbc
 * @param secret 密钥
 * @param iv
 * @returns 加密后的 hex 字符串
 */
export function encrypt(
  str: string,
  algorithm: string = ALG,
  secret: string = KEY,
  iv: BinaryLike = IV,
  inputEncoding: Encoding = 'utf8',
  ouputEncoding: Encoding = 'hex',
): string {
  const cipher = createCipheriv(algorithm, secret, iv);
  const enc: string = cipher.update(str, inputEncoding, ouputEncoding);
  return enc + cipher.final(ouputEncoding);
}

/**
 * 字符串解密
 * @param str 待解密的字符串
 * @param algorithm 加密算法，默认 aes-256-cbc
 * @param secret 密钥
 * @param iv
 * @returns 解密后的字符串
 */
export function decrypt(
  str: string,
  algorithm: string = ALG,
  secret: string = KEY,
  iv: BinaryLike = IV,
  inputEncoding: Encoding = 'hex',
  ouputEncoding: Encoding = 'utf8',
): string {
  const decipher = createDecipheriv(algorithm, secret, iv);
  const dec: string = decipher.update(str, inputEncoding, ouputEncoding);
  return dec + decipher.final(ouputEncoding);
}

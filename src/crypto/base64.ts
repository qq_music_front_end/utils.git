/** Base64 编码 */
export function encodeBase64(str: string): string {
  if (!str) {
    return '';
  }

  // 浏览器环境使用
  if (typeof window !== 'undefined') {
    const bytes: Uint8Array = new TextEncoder().encode(str);
    const binString: string = String.fromCodePoint(...bytes);
    return btoa(binString);
  }

  // Node 环境使用
  const buf: Buffer = Buffer.from(str, 'utf-8');
  return buf.toString('base64');
}

/** Base64 解码 */
export function decodeBase64(base64: string): string {
  if (!base64) {
    return '';
  }

  // 浏览器环境使用
  if (typeof window !== 'undefined') {
    const binString: string = atob(base64);
    const bytes: Uint8Array = Uint8Array.from(binString, (m: string): number => m.codePointAt(0));
    return new TextDecoder().decode(bytes);
  }

  // Node 环境使用
  const buf: Buffer = Buffer.from(base64, 'base64');
  return buf.toString();
}

# 变更日志

## 3.0.4

### 特性

- [clamp](./src/math/clamp.ts) 增加方法

## 3.0.3

### 优化

- [decodeBase64](./src/crypto/base64.ts) 支持浏览器环境调用
- [encodeBase64](./src/crypto/base64.ts) 支持浏览器环境调用

## 3.0.2

### 特性

- [encodeHTML](./src/html/encodeHTML.ts) 优化转换逻辑，支持自定义转换规则

## 3.0.1

### 特性

- [createRandomText](./src/object/createRandomText.ts) 增加方法

## 3.0.0

### 特性

- 调整方法导出逻辑，优化第三方引用后构建大小

## 2.1.1

### 特性

- [compress](./src/crypto/compress.ts) 修复最大只能解压 65533 字节的问题

## 2.1.0

### 优化

- [parse](./src/object/parse.ts) 优化实现逻辑

## 2.0.9

### 特性

- [compress](./src/crypto/compress.ts) 增加方法
- [decompress](./src/crypto/compress.ts) 增加方法

## 2.0.8

### 特性

- [sendRequest](./src/request/node.ts) 增加方法
- [loadResponse](./src/request/node.ts) 增加方法

### 优化

- [UA](./src/object/ua.ts) 调整 Windows 系统版本号解析逻辑

### 修复

- [UA](./src/object/ua.ts) 修复解析时报错的问题

## 2.0.7

### 优化

- `createHTML` 支持数组子元素

## 2.0.6

### 修复

- 修复 `getJSONP` 接口使用相对路径链接时报错的问题

## 2.0.5

### 优化

- 完善注释，增加接口文档说明页

## 2.0.4

### 特性

- 增加 `parseSearchParams` 方法

## 2.0.3

### 修复

- 修复 `createElement` 方法不支持数组类型子节点的问题

## 2.0.2

### 特性

- 增加 `parallel` 方法

## 2.0.1

### 修复

- 修复 `url.join` 方法异常报错

## 2.0.0

### 特性

- 使用 TypeScript 重构

## 1.2.5

### 特性

- 增加 `getRandomItem` 方法

## 1.2.4

### 特性

- 增加 `debounce` 方法
- 增加 `extend` 方法
- 增加 `throttle` 方法

### 优化

- `dom.create` 方法支持绑定事件

## 1.2.3

### 优化

- `dom.create` 方法支持创建 SVG 元素

## 1.2.2

### 特性

- 增加 `isPromise` 方法
- 增加 `series` 方法

## 1.2.1

### 优化

- 优化 `getJSONP` 方法缓存逻辑

## 1.2.0

### 特性

- 增加文本比较方法 `diff`
- 增加存储模块 `storage`，支持内存、localStorage、sessionStorage 和 indexedDB

## 1.1.4

### 优化

- 选取文件时增加支持设置文件类型

## 1.1.3

### 优化

- 改用 https 协议请求统一 cgi

## 1.1.2

### 修复

- 修复 `uajax` 方法中 `comm` 参数不生效的问题

## 1.1.1

### 优化

- `loadUrl` 和 `pickFile` 方法改为 async function

## 1.1.0

### 修复

- 修复 `ajax` 方法在 get 请求时丢失参数的问题

## 1.0.9

### 特性

- 增加 ajax 请求方法 `ajax`

### 修复

- 修复 `dom.triggerEvent` 方法不生效的问题

## 1.0.8

### 修复

- 修复复用 `uajax` 对象时请求报错的问题

## 1.0.7

### 修复

- 修复复用 `JSONP` 请求时，第二次请求返回为空的问题

## 1.0.5

### 优化

- `getJSONP` 方法默认支持缓存

## 1.0.4

### 修复

- 修复统一 CGI 请求时，`comm` 参数没有生效的问题

## 1.0.3

### 特性

- 增加参数序列化方法 `serializeParam`
- 增加统一 CGI 请求方法 `uajax`

### 优化

- `getJSONP` 方法增加支持 `data` 参数

## 1.0.0

### 特性

- 提交项目

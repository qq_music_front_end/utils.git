# 工具库

## 安装

```sh
npm i -S @qmfe/utils
```

## 使用

```js
import { isFunction } from '@qmfe/utils';

if (isFunction(fn)) {
  // todo
}
```

## 方法列表

### 加解密/压缩

| 文件                 | 方法 / 类    | 浏览器 | Node | 说明            |
| -------------------- | ------------ | ------ | ---- | --------------- |
| crypto/base64.ts     | decodeBase64 | ✔️     | ✔️   | Base64 解码     |
| crypto/base64.ts     | encodeBase64 | ✔️     | ✔️   | Base64 编码     |
| crypto/compress.ts   | compress     | ✔️     | ✔️   | LZSS 字符串压缩 |
| crypto/compress.ts   | decompress   | ✔️     | ✔️   | LZSS 字符串解压 |
| crypto/createHash.ts | createHash   | ❌     | ✔️   | 计算哈希值      |
| crypto/encrypt.ts    | decrypt      | ❌     | ✔️   | 字符串解密      |
| crypto/encrypt.ts    | encrypt      | ❌     | ✔️   | 字符串加密      |

### 日期

| 文件          | 方法 / 类  | 浏览器 | Node | 说明             |
| ------------- | ---------- | ------ | ---- | ---------------- |
| date/index.ts | formatDate | ✔️     | ✔️   | 格式化日期字符串 |
| date/index.ts | formatTime | ✔️     | ✔️   | 格式化时间字符串 |

### DOM 操作

| 文件                 | 方法 / 类        | 浏览器 | Node | 说明              |
| -------------------- | ---------------- | ------ | ---- | ----------------- |
| dom/createElement.ts | createElement    | ✔️     | ❌   | 创建 DOM 节点     |
| dom/onScroll.ts      | onScroll         | ✔️     | ❌   | 监听页面滚动事件  |
| dom/pickFile.ts      | pickFile         | ✔️     | ❌   | 选取文件          |
| dom/querySelector.ts | querySelector    | ✔️     | ❌   | 查询 DOM 节点     |
| dom/querySelector.ts | querySelectorAll | ✔️     | ❌   | 查询 DOM 节点列表 |
| dom/ready.ts         | ready            | ✔️     | ❌   | 等待页面可交互    |
| dom/setAttributes.ts | setAttributes    | ✔️     | ❌   | 设置节点属性      |
| dom/togglePostion.ts | togglePostion    | ✔️     | ❌   | 交换两个元素位置  |
| dom/triggerEvent.ts  | triggerEvent     | ✔️     | ❌   | 触发 DOM 事件     |

### 控制流

| 文件             | 方法 / 类 | 浏览器 | Node | 说明             |
| ---------------- | --------- | ------ | ---- | ---------------- |
| flow/debounce.ts | debounce  | ✔️     | ✔️   | 防抖             |
| flow/parallel.ts | parallel  | ✔️     | ✔️   | 并行流程控制     |
| flow/series.ts   | series    | ✔️     | ✔️   | 串行流程控制     |
| flow/sleep.ts    | sleep     | ✔️     | ✔️   | 延迟执行         |
| flow/sync.ts     | sync      | ✔️     | ✔️   | 以同步的方式执行 |
| flow/throttle.ts | throttle  | ✔️     | ✔️   | 节流             |

### HTML

| 文件               | 方法 / 类  | 浏览器 | Node | 说明           |
| ------------------ | ---------- | ------ | ---- | -------------- |
| html/createHTML.ts | createHTML | ✔️     | ✔️   | 生成 HTML 文本 |
| html/decodeHTML.ts | decodeHTML | ✔️     | ✔️   | HTML 解码      |
| html/encodeHTML.ts | encodeHTML | ✔️     | ✔️   | HTML 编码      |

### 数学运算相关

| 文件          | 方法 / 类 | 浏览器 | Node | 说明                   |
| ------------- | --------- | ------ | ---- | ---------------------- |
| math/clamp.ts | clamp     | ✔️     | ✔️   | 将数值限制在指定范围内 |

### 对象解析 / 扩展

| 文件                       | 方法 / 类        | 浏览器 | Node | 说明                        |
| -------------------------- | ---------------- | ------ | ---- | --------------------------- |
| object/createRandomText.ts | createRandomText | ✔️     | ✔️   | 生成随机字符串              |
| object/encodeForm.ts       | encodeForm       | ✔️     | ➖   | 普通对象转为表单对象        |
| object/extend.ts           | extend           | ✔️     | ✔️   | 扩展对象                    |
| object/getRandomItem.ts    | getRandomItem    | ✔️     | ✔️   | 随机获取数组中的一个元素    |
| object/parse.ts            | parseJSON        | ✔️     | ✔️   | 解析 JSON 字符串            |
| object/parse.ts            | parse            | ✔️     | ✔️   | 从字符串中解析对象          |
| object/parse.ts            | parseExports     | ✔️     | ✔️   | 从字符串中解析 exports 对象 |
| object/parseNumUnit.ts     | parseNumUnit     | ✔️     | ✔️   | 解析计数单位                |
| object/toString.ts         | toString         | ✔️     | ✔️   | 将对象转为字符串            |
| object/ua.ts               | UA               | ✔️     | ✔️   | UA 解析                     |

### 网络请求

| 文件                   | 方法 / 类    | 浏览器 | Node | 说明              |
| ---------------------- | ------------ | ------ | ---- | ----------------- |
| request/getJSONP.ts    | getJSONP     | ✔️     | ❌   | 发送 JSONP 请求   |
| request/loadUrl.ts     | loadUrl      | ✔️     | ❌   | 加载 CSS 或 JS    |
| request/sendRequest.ts | loadResponse | ❌     | ✔️   | 解析请求返回      |
| request/sendRequest.ts | sendRequest  | ❌     | ✔️   | 发送网络请求      |
| request/uajax.ts       | uajax        | ✔️     | ❌   | 发送统一 CGI 请求 |

### 存储

| 文件             | 方法 / 类 | 浏览器 | Node | 说明           |
| ---------------- | --------- | ------ | ---- | -------------- |
| store/cookie.ts  | cookie    | ✔️     | ❌   | 操作 Cookie    |
| store/idb.ts     | idb       | ✔️     | ❌   | 操作 indexedDB |
| store/memory.ts  | memory    | ✔️     | ✔️   | 操作内存缓存   |
| store/storage.ts | storage   | ✔️     | ❌   | 操作本地存储   |

### 控制台样式

| 文件           | 方法 / 类       | 浏览器 | Node | 说明                      |
| -------------- | --------------- | ------ | ---- | ------------------------- |
| style/index.ts | reset           | ✔️     | ✔️   | 控制台文本样式 - 重置样式 |
| style/index.ts | bold            | ✔️     | ✔️   | 控制台文本样式 - 加粗     |
| style/index.ts | italic          | ✔️     | ✔️   | 控制台文本样式 - 斜体     |
| style/index.ts | underline       | ✔️     | ✔️   | 控制台文本样式 - 下划线   |
| style/index.ts | inverse         | ✔️     | ✔️   | 控制台文本样式 - 反转颜色 |
| style/index.ts | hidden          | ✔️     | ✔️   | 控制台文本样式 - 隐藏     |
| style/index.ts | strikethrough   | ✔️     | ✔️   | 控制台文本样式 - 删除线   |
| style/index.ts | white           | ✔️     | ✔️   | 控制台前景色 - 白色       |
| style/index.ts | black           | ✔️     | ✔️   | 控制台前景色 - 黑色       |
| style/index.ts | red             | ✔️     | ✔️   | 控制台前景色 - 红色       |
| style/index.ts | green           | ✔️     | ✔️   | 控制台前景色 - 绿色       |
| style/index.ts | yellow          | ✔️     | ✔️   | 控制台前景色 - 黄色       |
| style/index.ts | blue            | ✔️     | ✔️   | 控制台前景色 - 蓝色       |
| style/index.ts | magenta         | ✔️     | ✔️   | 控制台前景色 - 洋红色     |
| style/index.ts | cyan            | ✔️     | ✔️   | 控制台前景色 - 青色       |
| style/index.ts | gray            | ✔️     | ✔️   | 控制台前景色 - 灰色       |
| style/index.ts | brightRed       | ✔️     | ✔️   | 控制台前景色 - 亮红色     |
| style/index.ts | brightGreen     | ✔️     | ✔️   | 控制台前景色 - 亮绿色     |
| style/index.ts | brightYellow    | ✔️     | ✔️   | 控制台前景色 - 亮黄色     |
| style/index.ts | brightBlue      | ✔️     | ✔️   | 控制台前景色 - 亮蓝色     |
| style/index.ts | brightMagenta   | ✔️     | ✔️   | 控制台前景色 - 淡红色     |
| style/index.ts | brightCyan      | ✔️     | ✔️   | 控制台前景色 - 亮青色     |
| style/index.ts | bgWhite         | ✔️     | ✔️   | 控制台背景色 - 白色       |
| style/index.ts | bgBlack         | ✔️     | ✔️   | 控制台背景色 - 黑色       |
| style/index.ts | bgRed           | ✔️     | ✔️   | 控制台背景色 - 红色       |
| style/index.ts | bgGreen         | ✔️     | ✔️   | 控制台背景色 - 绿色       |
| style/index.ts | bgYellow        | ✔️     | ✔️   | 控制台背景色 - 黄色       |
| style/index.ts | bgBlue          | ✔️     | ✔️   | 控制台背景色 - 蓝色       |
| style/index.ts | bgMagenta       | ✔️     | ✔️   | 控制台背景色 - 洋红色     |
| style/index.ts | bgCyan          | ✔️     | ✔️   | 控制台背景色 - 青色       |
| style/index.ts | bgBrightRed     | ✔️     | ✔️   | 控制台背景色 - 亮红色     |
| style/index.ts | bgBrightGreen   | ✔️     | ✔️   | 控制台背景色 - 亮绿色     |
| style/index.ts | bgBrightYellow  | ✔️     | ✔️   | 控制台背景色 - 亮黄色     |
| style/index.ts | bgBrightBlue    | ✔️     | ✔️   | 控制台背景色 - 亮蓝色     |
| style/index.ts | bgBrightMagenta | ✔️     | ✔️   | 控制台背景色 - 淡红色     |
| style/index.ts | bgBrightCyan    | ✔️     | ✔️   | 控制台背景色 - 亮青色     |

### 类型检测

| 文件                | 方法 / 类   | 浏览器 | Node | 说明             |
| ------------------- | ----------- | ------ | ---- | ---------------- |
| type/checkType.ts   | checkType   | ✔️     | ✔️   | 对象类型检测     |
| type/isArray.ts     | isArray     | ✔️     | ✔️   | 是否是 Array     |
| type/isBoolean.ts   | isBoolean   | ✔️     | ✔️   | 是否是 Boolean   |
| type/isDate.ts      | isDate      | ✔️     | ✔️   | 是否是 Date      |
| type/isEmpty.ts     | isEmpty     | ✔️     | ✔️   | 是否是 Empty     |
| type/isFile.ts      | isFile      | ✔️     | ✔️   | 是否是 File      |
| type/isFormData.ts  | isFormData  | ✔️     | ✔️   | 是否是 FormData  |
| type/isFunction.ts  | isFunction  | ✔️     | ✔️   | 是否是 Function  |
| type/isNull.ts      | isNull      | ✔️     | ✔️   | 是否是 Null      |
| type/isNumber.ts    | isNumber    | ✔️     | ✔️   | 是否是 Number    |
| type/isObject.ts    | isObject    | ✔️     | ✔️   | 是否是 Object    |
| type/isPromise.ts   | isPromise   | ✔️     | ✔️   | 是否是 Promise   |
| type/isRegExp.ts    | isRegExp    | ✔️     | ✔️   | 是否是 RegExp    |
| type/isString.ts    | isString    | ✔️     | ✔️   | 是否是 String    |
| type/isTrueEmpty.ts | isTrueEmpty | ✔️     | ✔️   | 是否是 TrueEmpty |
| type/isUndefined.ts | isUndefined | ✔️     | ✔️   | 是否是 Undefined |

### 链接操作

| 文件                     | 方法 / 类         | 浏览器 | Node | 说明          |
| ------------------------ | ----------------- | ------ | ---- | ------------- |
| url/joinUrl.ts           | joinUrl           | ✔️     | ✔️   | 合并链接      |
| url/parseSearchParams.ts | parseSearchParams | ✔️     | ✔️   | 解析 URL 参数 |
